<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOrder extends Model
{
    protected $table = "temp_order";
    protected $fillable= [
        'alamat_penerima','nama_penerima', 'no_hp', 'opsi_pengiriman', 'ket_pengiriman', 'harga_pengiriman', 'sub_total', 'total_bayar'
    ];
}
