<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodePengiriman extends Model
{
    protected $table = "tb_metod_pengiriman";
    protected $fillable= [
        'nama_pengiriman'
    ];
    public function pesanan(){
        return $this->hasMany('App\Pesanan','id_pesanan');
    }
}
