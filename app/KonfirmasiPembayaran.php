<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KonfirmasiPembayaran extends Model
{
    protected $table = "confirmation_payment";
    protected $fillable= [
        'transfer_destination', 'name_order', 'account_owner', 'date_payment', 'total_payment', 'np_hp', 'image_confirmation', 'pesanan_id'
    ];
    public function pesanan(){
        return $this->belongsTo(Pesanan::class,'pesanan_id');
    }
}
