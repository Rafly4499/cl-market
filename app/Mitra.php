<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
    protected $table = "tb_pedagang";
    protected $fillable= [
        'id_user', 'nama_lengkap', 'no_hp', 'sex', 'photo'
    ];
    public function user(){
        return $this->belongsTo('App\User','id_user');
    }
    public function toko(){
        return $this->belongsTo('App\Toko','id_toko');
    }
}
