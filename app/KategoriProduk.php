<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    protected $table = "tb_kategori_produk";
    protected $fillable= [
        'jenis_produk_id', 'kategori_produk', 'image_kategori'
    ];
    public function produk(){
        return $this->hasMany('App\Produk');
    }
    public function jenis_produk(){
        return $this->belongsTo('App\JenisProduk','jenis_produk_id');
    }
}
