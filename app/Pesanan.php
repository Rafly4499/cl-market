<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $table = "tb_pesanan";
    protected $fillable= [
        'no_order', 'id_pedagang', 'id_customer', 'alamat_pengiriman', 'metode_pengiriman', 'status', 'biaya_kirim', 'total_hargaproduk', 'date_order', 'date_payment', 'date_delivery', 'date_done', 'date_limit', 'total_harga', 'catatan_order'
    ];
    public function pedagang(){
        return $this->belongsTo('App\Mitra','id_pedagang');
    }
    public function customer(){
        return $this->belongsTo('App\Customer','id_customer');
    }
    public function detailpesanan(){
        return $this->hasMany('App\DetailPesanan','id_pesanan');
    }
}
