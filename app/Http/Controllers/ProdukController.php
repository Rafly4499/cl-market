<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\JenisProduk;
use App\KategoriProduk;
use App\Toko;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Image;
use File;
class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        return view('backend.pages.produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Produk::orderBy('id')->get();
        $produk = Produk::all();
        $jenis_produk = JenisProduk::all();
        $kategori_produk = KategoriProduk::all();
        $toko = Toko::all();
        return view('backend.pages.produk.create', compact('produk','jenis_produk','kategori_produk','toko'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
      
                
                      if ($request->hasFile('image')) {
                        if ($request->file('image')->isValid()) {
                            $destinationPath = 'img/'; // upload path
                            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                            $fileName = rand(11111,99999).'.'.$extension; // renaming image
                            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                                  $constraint->aspectRatio();
                                  $constraint->upsize();
                              })->save($destinationPath.$fileName);
                            $data['gambar'] = $fileName;
                            unset($data['image']);
                        }
                      }
                
          $data = Produk::create($data);
                      
        
                Session::flash('message', $data['nama_produk'] . ' added successfully');
                return redirect('/admin-ds/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $jenis_produk = JenisProduk::all();
        $kategori_produk = KategoriProduk::all();
        $toko = Toko::all();
        return view('backend.pages.produk.edit', compact('produk','jenis_produk', 'kategori_produk', 'toko'));
    }
    public function view($id)
    {
        $produk = Produk::find($id);
        return view('backend.pages.produk.view', compact('produk'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
            $req['gambar'] = $fileName;
            unset($req['image']);
    
              $result = Produk::find($id);
              if (!empty($result->gambar)) {
                File::delete('img/'.$result->gambar);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
        $data = Produk::where('id', $id)->update($req);

	    Session::flash('message', $data['nama_produk'] . ' updated successfully');
        return redirect('admin-ds/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Produk::find($id);
	    $data->destroy($id);

	    Session::flash('message', $data['nama_produk'] . ' deleted successfully');
	    return redirect('admin-ds/produk');
    }
}
