<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReviewProduk;
use Illuminate\Support\Facades\Session;
class ReviewProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $review = ReviewProduk::all();
        return view('backend.pages.reviewproduk.index', compact('review'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = ReviewProduk::find($id);
        return view('backend.pages.reviewproduk.edit', ['review' => $review]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ReviewProduk::find($id);
        $review = $request->all();
        $data->update($review);

	    Session::flash('message', 'Updated status successfully');
        return redirect('/admin-ds/review');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ReviewProduk::find($id);
	    $data->destroy($id);
	    Session::flash('message', 'Deleted successfully');
	    return redirect('/admin-ds/review');
    }
}
