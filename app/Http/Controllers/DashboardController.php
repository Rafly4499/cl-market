<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesanan;
use App\Customer;
use App\Mitra;
use App\Toko;
use App\Produk;
use App\KategoriProduk;
use App\JenisProduk;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        $countordermasuk = Pesanan::where('status','Belum Bayar')->get()->count();
        $countorderkirim = Pesanan::where('status','Dikirim')->get()->count();
        $countorderbatal = Pesanan::where('status','Dibatalkan')->get()->count();
        $countorderselesai = Pesanan::where('status','Selesai')->get()->count();
        $countcustomer = Customer::all()->count();
        $countmitra = Mitra::all()->count();
        $counttoko = Toko::all()->count();
        $countprodukaktif = Produk::where('status','1')->get()->count();
        $countproduktidakaktif = Produk::where('status','0')->get()->count();
        $countcategory = KategoriProduk::all()->count();
        $countjenis = JenisProduk::all()->count();
        $countuser = User::all()->count();
        $countusernonaktif = User::where('status','Tidak Aktif')->get()->count();
        $countorder = Pesanan::all()->count();
        $countproduk = Produk::all()->count();
        return view('backend.pages.dashboard',
        compact(
            'countuser',
            'countusernonaktif',
            'countorder',
            'countproduk',
            'countordermasuk',
            'countorderkirim', 
            'countorderbatal',
            'countorderselesai',
            'countcustomer',
            'countmitra',
            'counttoko',
            'countprodukaktif',
            'countproduktidakaktif',
            'countcategory',
            'countjenis',
        ));
    }
}
