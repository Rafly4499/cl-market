<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriProduk;
use App\JenisProduk;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Image;
use File;
class KategoriProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = KategoriProduk::all();
        return view('backend.pages.kategoriproduk.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_produk = JenisProduk::all();
        $data = KategoriProduk::orderBy('id')->get();
        $kategori = KategoriProduk::all();
        return view('backend.pages.kategoriproduk.create', compact('kategori','jenis_produk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
      
                
        if ($request->hasFile('image')) {
          if ($request->file('image')->isValid()) {
              $destinationPath = 'img/'; // upload path
              $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
              $fileName = rand(11111,99999).'.'.$extension; // renaming image
              $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
              Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($destinationPath.$fileName);
              $data['image_kategori'] = $fileName;
              unset($data['image']);
          }
        }
  
$data = KategoriProduk::create($data);
        

  Session::flash('success', $data['kategori_produk'] . ' added successfully');
  return redirect('/admin-ds/kategoriproduk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $jenis_produk = JenisProduk::all();
        $kategori = KategoriProduk::find($id);
        return view('backend.pages.kategoriproduk.edit', compact('jenis_produk', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
            $req['image_kategori'] = $fileName;
            unset($req['image']);
    
              $result = KategoriProduk::find($id);
              if (!empty($result->image_kategori)) {
                File::delete('img/'.$result->image_kategori);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
        $data = KategoriProduk::where('id', $id)->update($req);

	    Session::flash('success', $data['kategori_produk'] . ' updated successfully');
        return redirect('admin-ds/kategoriproduk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = KategoriProduk::find($id);
	    $data->destroy($id);

	    Session::flash('message', $data['kategori_produk'] . ' deleted successfully');
	    return redirect('/admin-ds/kategoriproduk');
    }
}
