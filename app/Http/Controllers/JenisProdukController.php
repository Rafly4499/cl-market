<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisProduk;
use Illuminate\Support\Facades\Session;
class JenisProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis = JenisProduk::all();
        return view('backend.pages.jenisproduk.index', compact('jenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = JenisProduk::orderBy('id')->get();
        $jenis = JenisProduk::all();
        return view('backend.pages.jenisproduk.create', compact('jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
	    JenisProduk::create($data);
	    Session::flash('message', $data['jenis_produk'] . ' added successfully');
	    return redirect('/admin-ds/jenisproduk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = JenisProduk::find($id);
        return view('backend.pages.jenisproduk.edit', ['jenis' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = JenisProduk::find($id);
        $jenis = $request->all();
        $data->update($jenis);

	    Session::flash('message', $data['jenis_produk'] . ' updated successfully');
        return redirect('/admin-ds/jenisproduk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = JenisProduk::find($id);
	    $data->destroy($id);
	    Session::flash('message', $data['jenis_produk'] . ' deleted successfully');
	    return redirect('/admin-ds/jenisproduk');
    }
}
