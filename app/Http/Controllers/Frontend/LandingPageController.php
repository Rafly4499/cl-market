<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Produk;
use App\JenisProduk;
use App\KategoriProduk;
class LandingPageController extends Controller
{
    public function index(){
        $kerajinan = Produk::where('id_jenis_produk',7)->orderBy('id','desc')->get();
        $buah = Produk::where('id_jenis_produk',6)->orderBy('id','desc')->get();
        $batik = Produk::where('id_jenis_produk',5)->orderBy('id','desc')->get();
        $sayur = Produk::where('id_jenis_produk',4)->orderBy('id','desc')->get();
        $product = Produk::where('status','1')->orderBy('id','desc')->get();
        $countproduk = Produk::where('status','1')->get()->count();
        $category = KategoriProduk::all();
        return view('frontend.pages.landingpage.index',compact('kerajinan', 'buah', 'batik', 'category','product'));
    }
    public function category($id){
        $product = Produk::where('status','1')->where('id_kategori_produk',$id)->orderBy('id','desc')->get();
        $countproduk = Produk::where('status','1')->get()->count();
        $category = KategoriProduk::where('id', $id)->first();
        $allcategory = KategoriProduk::all();
        return view('frontend.pages.landingpage.category',compact('product', 'countproduk', 'category', 'allcategory'));
    }
    public function product($id){
        $product = Produk::where('status','1')->where('id_kategori_produk',$id)->orderBy('id','desc')->get();
        $countproduk = Produk::where('status','1')->get()->count();
        $category = KategoriProduk::where('id', $id)->first();
        $allcategory = KategoriProduk::all();
        return view('frontend.pages.landingpage.product',compact('product', 'countproduk', 'category', 'allcategory'));
    }
}
