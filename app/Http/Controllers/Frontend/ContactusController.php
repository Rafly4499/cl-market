<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;
use App\Contactus;
class ContactusController extends Controller
{
    public function index(){
        return view('frontend.pages.contactus.index');            
    }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
        $this->validate($request, [
            'subject' => 'required',
            'email' => 'required|email',
            'message' => 'required'
            ]);
         $data = $request->all();
         Contactus::create($data);
         Session::flash('info', ' Pesan Anda Telah Berhasil Terkirim');
         return redirect('/contactus');
     }
}
