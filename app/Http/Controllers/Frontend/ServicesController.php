<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Produk;
use App\JenisProduk;
use App\KategoriProduk;
use App\ReviewProduk;
use Auth;
use App\Keranjang;
use App\Pesanan;
use App\DetailPesanan;
use App\KonfirmasiPembayaran;
use App\Customer;
use DateTime;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Image;
use File;
use App\Mail\OrderMail;
use Illuminate\Support\Facades\Mail;
class ServicesController extends Controller
{
    public function shop(){
        $product = Produk::where('status','1')->orderBy('id','desc')->paginate(9);
        $countproduk = Produk::where('status','1')->get()->count();
        $jenisItems = JenisProduk::with('kategori_produk')->get();
        return view('frontend.pages.layanan.pasar',compact('product','countproduk','jenisItems'));        
    }
    public function detailProduk($id){
        $product = Produk::where('id',$id)->first();
        $jenisItems = JenisProduk::with('kategori_produk')->get();
        $count_review = ReviewProduk::where('id_produk',$id)->where('status','1')->orderBy('id','desc')->count();
        $review = ReviewProduk::where('id_produk',$id)->limit(10)->where('status','1')->orderBy('id','desc')->get();
        $lainnya = Produk::where('id_kategori_produk',$product->id_kategori_produk)->where('status','1')->limit(4)->orderBy('id', 'ASC')->get();
        return view('frontend.pages.layanan.detailproduk',compact('product','jenisItems','count_review','lainnya','review'));        
    }
    
    public function searchProduct(Request $request){
        $search = $request->search;
        $product = Produk::where('nama_produk','like',"%".$search."%")->where('status','1')->paginate(9);
        $jenisItems = JenisProduk::with('kategori_produk')->get();
        $countproduk = Produk::where('nama_produk','like',"%".$search."%")->where('status','1')->get()->count();
        return view('frontend.pages.layanan.searchproduk',compact('product','jenisItems','search','countproduk'));
    }
    public function sorting(Request $request){
        if($request->sort == "Produk Termahal"){
            $product = Produk::where('status','1')->orderBy('harga','DESC')->paginate(9);
            $jenisItems = JenisProduk::with('kategori_produk')->get();
            $countproduk = Produk::where('status','1')->orderBy('harga','DESC')->get()->count();
        return view('frontend.pages.layanan.sortingproduk',compact('product','jenisItems','countproduk'));
        }else if($request->sort == "Produk Termurah"){
            $product = Produk::where('status','1')->orderBy('harga','ASC')->paginate(9);
            $jenisItems = JenisProduk::with('kategori_produk')->get();
            $countproduk = Produk::where('status','1')->orderBy('harga','ASC')->get()->count();
            return view('frontend.pages.layanan.sortingproduk',compact('product','jenisItems','countproduk'));
        }else if($request->sort == "Produk Terbaru"){
            $product = Produk::where('status','1')->orderBy('id','DESC')->paginate(9);
            $jenisItems = JenisProduk::with('kategori_produk')->get();
            $countproduk = Produk::where('status','1')->orderBy('id','DESC')->get()->count();
            return view('frontend.pages.layanan.sortingproduk',compact('product','jenisItems','countproduk'));
        }else{
        }
        
    }
    public function jenisproduk($id){
        if($id){
            $jenis = Jenisproduk::where('id', $id)->first();
            if(!$jenis){
                abort(404);
            }
        }
        
        $product = Produk::where('status','1')->where('id_jenis_produk','=',$jenis->id)->paginate(9);
        $jenisItems = JenisProduk::with('kategori_produk')->get();
        $countproduk = Produk::where('status','1')->where('id_jenis_produk','=',$jenis->id)->get()->count();
        return view('frontend.pages.layanan.jenisproduk',compact('jenis','product','jenisItems','countproduk'));
        
    }
    public function kategoriproduk($id){
        if($id){
            $kategori = KategoriProduk::where('id', $id)->first();
            if(!$kategori){
                abort(404);
            }
        }
        
        $product = Produk::where('status','1')->where('id_kategori_produk','=',$kategori->id)->paginate(9);
        $jenisItems = JenisProduk::with('kategori_produk')->get();
        $countproduk = Produk::where('status','1')->where('id_kategori_produk','=',$kategori->id)->get()->count();
        return view('frontend.pages.layanan.kategoriproduk',compact('kategori','product','jenisItems','countproduk'));
        
    }
    public function reviewProduk(Request $request)
    {
        $data = $request->all();
	    ReviewProduk::create($data);
	    Session::flash('success', 'Review berhasil masuk');
	    return redirect()->back()->with('successreview','Berhasil');
    }
    public function cart($id)
    {
        $cart = Keranjang::where('id_customer', $id)->orderBy('id', 'DESC')->get();
        $lainnya = Produk::where('status','1')->limit(4)->orderBy('harga', 'DESC')->get();
        return view('frontend.pages.layanan.keranjang',compact('cart','lainnya'));
    }
    public function submitCart(Request $request){
        $produk = Produk::where('id', request('id_produk'))->first();
        $cekcart = Keranjang::where('id_produk', request('id_produk'))->first();
        $cekhavedata = Keranjang::where('id_customer', Auth::guard('customer')->user()->id)->first();
        if ($cekhavedata) {
            $cekproduk = Produk::where('id', $cekhavedata->id_produk)->first();
        // var_dump($cekcart->count());die();
            if ($produk->stok != 0) {
                if (request('jumlah') != 0) {
                    if ($cekcart) {
                        $updatejumlah = $cekcart->jumlah + request('jumlah');
                        $updateharga = $cekcart->sub_harga + request('sub_harga');
                        $updatecart = Keranjang::where('id_produk', request('id_produk'))->update([
                        'jumlah' => $updatejumlah,
                        'sub_harga' => $updateharga,
                    ]);

                    
                        if ($updatecart) {
                            $stokkurang = $produk->stok - request('jumlah');
                            $updatestok = Produk::where('id', request('id_produk'))->update([
                            'stok' => $stokkurang,
                        ]);
                        Session::flash('success', 'Produk berhasil ditambahkan kedalam keranjang');
                        return redirect('/customer/cart/'.Auth::guard('customer')->user()->id);
                        } else {
                            return redirect()->back()->with('gagal','Permintaan anda gagal');
                        }
                    } else {
                        $cart = Keranjang::create([
                        'id_customer' => Auth::guard('customer')->user()->id,
                        'id_produk' => request('id_produk'),
                        'jumlah' => request('jumlah'),
                        'sub_harga' => request('sub_harga'),
                        ]);
                    
                        if ($cart) {
                            $stokkurang = $produk->stok - request('jumlah');
                            $updatestok = Produk::where('id', request('id_produk'))->update([
                            'stok' => $stokkurang,
                        ]);
                        Session::flash('success', 'Produk berhasil ditambahkan kedalam keranjang');
                        return redirect('/customer/cart/'.Auth::guard('customer')->user()->id);
                        } else {
                            Session::flash('error', 'Permintaan anda gagal');
                            return redirect()->back();
                        }
                    }
                } else {
                    Session::flash('error', 'Tambahkan jumlah produk yang ingin dibeli');
                    return redirect()->back();
                }
            } else {
                Session::flash('error', 'Stok Kosong');
                return redirect()->back();
            }
    }else{
        if ($produk->stok != 0) {
            if (request('jumlah') != 0) {
                if ($cekcart) {
                    $updatejumlah = $cekcart->jumlah + request('jumlah');
                    $updateharga = $cekcart->sub_harga + request('sub_harga');
                    $updatecart = Keranjang::where('id_produk', request('id_produk'))->update([
                    'jumlah' => $updatejumlah,
                    'sub_harga' => $updateharga,
                ]);

                
                    if ($updatecart) {
                        $stokkurang = $produk->stok - request('jumlah');
                        $updatestok = Produk::where('id', request('id_produk'))->update([
                        'stok' => $stokkurang,
                    ]);
                    Session::flash('success', 'Produk berhasil ditambahkan kedalam keranjang');
                    return redirect('/customer/cart/'.Auth::guard('customer')->user()->id);
                    } else {
                        Session::flash('error', 'Permintaan anda gagal');
                        return redirect()->back();
                    }
                } else {
                    $cart = Keranjang::create([
                    'id_customer' => Auth::guard('customer')->user()->id,
                    'id_produk' => request('id_produk'),
                    'jumlah' => request('jumlah'),
                    'sub_harga' => request('sub_harga'),
                ]);
                
                    if ($cart) {
                        $stokkurang = $produk->stok - request('jumlah');
                        $updatestok = Produk::where('id', request('id_produk'))->update([
                        'stok' => $stokkurang,
                    ]);
                    Session::flash('success', 'Produk berhasil ditambahkan kedalam keranjang');
                    return redirect('/customer/cart/'.Auth::guard('customer')->user()->id);
                    } else {
                        Session::flash('error', 'Permintaan anda gagal');
                        return redirect()->back();
                    }
                }
            } else {
                Session::flash('error', 'Tambahkan jumlah produk yang ingin dibeli');
                return redirect()->back();
            }
        } else {
            Session::flash('error', 'Stok Kosong');
            return redirect()->back();
        }
    }
    }
    public function deleteCart($id)
    {
        $keranjang = Keranjang::where('id',$id)->first();
        $produk = Produk::where('id',$keranjang->id_produk)->first();
        $stoktambah = $produk->stok + $keranjang->jumlah;
        $updatestok = Produk::where('id',$keranjang->id_produk)->update([
            'stok' => $stoktambah,
        ]);
        $result = Keranjang::where('id',$id)->delete();
        if ($result) {
            return redirect()->back();
        } else {
            return redirect()->back()->with('gagal','Permintaan anda gagal');
        }
    }
    public function checkout($id)
    {
        $pesanan = Pesanan::where('no_order',$id)->first();
        $detpesanan = DetailPesanan::where('id_pesanan',$pesanan->id)->get();
        return view('frontend.pages.layanan.checkout',compact('pesanan','detpesanan'));
    }
    public function submitCheckout(Request $request){
        $customer = Customer::where('id', Auth::guard('customer')->user()->id)->first();
        $keranjang = Keranjang::where('id_customer', Auth::guard('customer')->user()->id)->get();
        $cek_keranjang = Keranjang::where('id_customer', Auth::guard('customer')->user()->id)->first();
        $cek_produk = Produk::where('id',$cek_keranjang->id_produk)->first();
        $lastdata = Pesanan::latest('id')->first();
        if($lastdata == NULL){
            $nextNoUrut = 1;    
        }else{
            $nextNoUrut = $lastdata->id + 1;
        }
        $today = date("Ymd");
        $nextNoTransaksi = $today.sprintf('%04s', $nextNoUrut);
        $specificToday = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
        $pesanan = Pesanan::create([
            'no_order' => $nextNoTransaksi,
            'id_customer' => $customer->id,
            'status' => 'Checkout',
            'total_hargaproduk' => request('subtotal_harga'),
            'total_harga' => request('total_pembayaran'),
        ]);
        foreach ($keranjang as $item) {
            $produk = Produk::where('id',$item->id_produk)->first();
            $det_pesanan = DetailPesanan::create([
                'id_pesanan' => $pesanan->id,
                'id_produk' => $item->id_produk,
                'quantity' => $item->jumlah,
                'harga_satuan' => $item->produk['harga'],
                'total_subharga' => $item->sub_harga,
            ]);
            $keranjang = Keranjang::where('id',$item->id)->delete();
        }
        if ($pesanan) {
            return redirect('/customer/checkout/'.$pesanan->no_order);
        } else {
            return redirect()->back()->with('gagal','Permintaan anda gagal');
        }
  
    }
    public function submitOrder(Request $request){
        $specificToday = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
        $updatepesanan = Pesanan::where('id', request('id_pesanan'))->update([
            'alamat_pengiriman' => request('alamat_pengiriman'),
            'status' => 'Belum Bayar',
            'date_order' => date('Y-m-d H:i:s'),
            'date_limit' => $specificToday->modify('+1 day'),
            'catatan_order' => request('catatan_order'),
        ]);
        $pesanan = Pesanan::where('id', request('id_pesanan'))->first();
        if ($updatepesanan) {
            try{
                Mail::to($pesanan->customer['email'])->send(new OrderMail( $pesanan ));
                Session::flash('success', 'Anda berhasil melakukan Order Produk. Silahkan cek email anda dan segera melakukan pembayaran');
                return redirect('/customer/invoice/'.$pesanan->id);
            }catch (Exception $e){
                return response (['status' => false,'errors' => $e->getMessage()]);
            }
            Session::flash('success', 'Anda berhasil melakukan booking. Silahkan cek email anda dan segera melakukan pembayaran');
            return redirect('/customer/invoice/'.$pesanan->id);
        } else {
            Session::flash('error', 'Permintaan anda gagal');
            return redirect()->back();
        }
  
    }
    public function viewinvoice($id)
    {
        $pesanan = Pesanan::where('id',$id)->first();
        $confirmation = KonfirmasiPembayaran::where('pesanan_id',$id)->first();
        $detpesanan = DetailPesanan::where('id_pesanan',$pesanan->id)->get();
        return view('frontend.pages.layanan.invoice',compact('confirmation','pesanan','detpesanan'));
    }
    public function PostConfirmationPayment(Request $request)
    {
        // $this->validate($request,[
        //     'g-recaptcha-response' => 'required|captcha'
        //     ]);
        
        $pesanan = Pesanan::where('no_order','LIKE','%'.request('pesanan_id').'%')->first();
        
        $data = new KonfirmasiPembayaran;
        $data->invoiceid = $request->invoiceid;
        $data->transfer_destination = $request->transfer_destination;
        $data->name_order = Auth::guard('customer')->user()->fullname;
        $data->account_owner = $request->account_owner;
        $data->date_payment = $request->date_payment;
        $data->total_payment = $request->total_payment;
        $data->no_hp = $request->no_hp;
        $data->pesanan_id = $pesanan->id;
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'img/konfirmasi_pembayaran/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
                $data['image_confirmation'] = $fileName;
            }
          }
        $confirm = $data->save();
        if ($confirm) {
            $result = Pesanan::where('id', $pesanan->id)->update([
            'status' => "Proses",
            ]);
        }
        Session::flash('success', 'Anda berhasil mengonfirmasi pembayaran. Silahkan menunggu konfirmasi dari admin');
        return redirect('/customer');
    }
    public function viewconfirmationPayment($id)
    {
        $pesanan = Pesanan::where('id',$id)->first();
        $detpesanan = DetailPesanan::where('id_pesanan',$pesanan->id)->get();
        return view('frontend.pages.layanan.konfirmasipembayaran',compact('pesanan','detpesanan'));
    }
    public function editData($id)
    {
        $account = Customer::find($id);
        return view('frontend.pages.landingpage.profile', ['account' => $account]);
    }
    public function updateCustomer(Request $request, $id)
{
    $data = Customer::find($id);
    $customer = $request->all();
    $data->update($customer);
    Session::flash('success', $data['fullname'] . ' updated successfully');
    return redirect('/');
}
public function pesananSelesai($id)
{
    $pesanan = Pesanan::where('id', $id)->update([
    'status' => "Selesai",
    'date_done' => date('Y-m-d H:i:s'),
    ]);
    if ($pesanan) {
        Session::flash('success', 'Order updated successfully');
        return redirect('/customer');
    }
}
}
