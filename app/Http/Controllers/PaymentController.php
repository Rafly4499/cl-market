<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
class PaymentController extends Controller
{
    public function getPayment(){
        $client = new Client();
        try{
            $res = $client->request('GET','http://192.168.43.6:8000/coba',[]);
            $data = json_decode($res->getBody()->getContents());
            return view('backend.pages.payment',['token'=> $data->token]);
        }catch(Exception $e){
            dd($e->getMessage());
        }
    }
    public function getTrans(){
        return view('backend.pages.midtrans');
    }
    
    
}
