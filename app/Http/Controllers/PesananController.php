<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesanan;
use App\DetailPesanan;
use App\KonfirmasiPembayaran;
use Illuminate\Support\Facades\Session;
class PesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pesanan = Pesanan::all();
		return view('backend.pages.pesanan.index')->with('pesanan',$pesanan);
    }
    public function rincian_pesanan($id)
    {
        $pesanan = Pesanan::find($id);
        $rincianpesanan = DetailPesanan::where('id_pesanan',$id)->get();
		return view('backend.pages.pesanan.rincianpesanan', compact('pesanan','rincianpesanan'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function view($id)
    {
        $pesanan = Pesanan::find($id);
		return view('backend.pages.pesanan.view')->with('pesanan',$pesanan);
    }
    public function approval($id)
    {
        $pesanan = Pesanan::where('id', $id)->update([
        'status' => "Sudah Bayar",
        'date_payment' => date('Y-m-d H:i:s'),
        ]);
        if ($pesanan) {
            Session::flash('info', 'Order updated successfully');
            return redirect('admin-ds/pesanan');
        }
    }
    public function rejected($id)
    {
        $pesanan = Pesanan::where('id', $id)->update([
        'status' => "Konfirmasi ulang",
        
        ]);
        if ($pesanan) {
            Session::flash('info', 'Order updated successfully');
            return redirect('admin-ds/pesanan');
        }
    }   
    public function detailpayment($id)
    {
        $pesanan = Pesanan::find($id);
        $confirmation = KonfirmasiPembayaran::where('pesanan_id',$id)->first();
		return view('backend.pages.pesanan.detailpayment', compact('pesanan','confirmation'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
