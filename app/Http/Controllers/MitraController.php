<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mitra;
use App\User;
use App\Toko;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Image;
use File;
class mitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mitra = mitra::all();
        return view('backend.pages.mitra.index', compact('mitra'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Mitra::orderBy('id')->get();
        $mitra = Mitra::all();
        
        return view('backend.pages.mitra.create', compact('mitra'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->is_admin = 0;
        $user->save();
        
        $mitra = new Mitra;
        $mitra->id_user = $user->id;
        $mitra->nama_lengkap = $request->nama_lengkap;
        $mitra->no_hp = $request->no_hp;
        $mitra->sex = $request->sex;
                      if ($request->hasFile('image')) {
                        if ($request->file('image')->isValid()) {
                            $destinationPath = 'img/'; // upload path
                            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                            $fileName = rand(11111,99999).'.'.$extension; // renaming image
                            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                                  $constraint->aspectRatio();
                                  $constraint->upsize();
                              })->save($destinationPath.$fileName);
                            $mitra->photo = $fileName;
                            unset($request->image);
                        }
                      }
        $mitra->save();
        $toko = new Toko;
        $toko->nama_toko = $request->nama_toko;
        $toko->id_pedagang = $mitra->id;
        $toko->save();
        Session::flash('message', 'Tambah Mitra successfully');
        return redirect('/admin-ds/mitra');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mitra = Mitra::find($id);
        $user = User::where('id', '=', $mitra->id_user);
        $toko = Toko::where('id_pedagang', '=', $id);
        return view('backend.pages.mitra.edit', compact('mitra','user', 'toko'));
    }
    public function view($id)
    {
        $mitra = Mitra::find($id);
        $user = User::where('id', '=', $mitra->id_user);
        $toko = Toko::where('id_pedagang', '=', $id);
        return view('backend.pages.mitra.edit', compact('mitra','user', 'toko'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mitra = Mitra::where('id', $id)->firstorfail();
        $mitra->nama_lengkap = $request->nama_lengkap;
        $mitra->no_hp = $request->no_hp;
        $mitra->sex = $request->sex;
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              })->save($destinationPath.$fileName);
              $mitra->photo = $fileName;
              unset($request->image);
    
              $result = Mitra::find($id);
              if (!empty($result->photo)) {
                File::delete('img/'.$result->photo);
              }
            }else {
              unset($request->image);
            }
          }else {
            unset($request->image);
          }
        $mitra->save();
        $user = User::where('id', $mitra->id_user)->firstorfail();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->is_admin = 0;
        $user->save();
        $toko = Toko::where('id_pedagang', $id)->firstorfail();
        $toko->nama_toko = $request->nama_toko;
        $toko->id_pedagang = $id;
        $toko->save();
	    Session::flash('message', 'Update Mitra successfully');
        return redirect('admin-ds/mitra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Mitra::find($id);
        $data->user()->delete();
	    $data->destroy($id);
	    Session::flash('message', 'Delete Mitra successfully');
	    return redirect('admin-ds/mitra');
    }
}
