<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MetodePembayaran;
use Illuminate\Support\Facades\Session;
class MetodePembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metode = MetodePembayaran::all();
        return view('backend.pages.metodepembayaran.index', compact('metode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = MetodePembayaran::orderBy('id')->get();
        $metode = MetodePembayaran::all();
        return view('backend.pages.metodepembayaran.create', compact('metode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
	    MetodePembayaran::create($data);
	    Session::flash('message', $data['nama_metode'] . ' added successfully');
	    return redirect('/admin-ds/metodepembayaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MetodePembayaran::find($id);
        return view('backend.pages.metodepembayaran.edit', ['metode' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = MetodePembayaran::find($id);
        $metode = $request->all();
        $data->update($metode);

	    Session::flash('message', $data['nama_metode'] . ' updated successfully');
        return redirect('/admin-ds/metodepembayaran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MetodePembayaran::find($id);
	    $data->destroy($id);

	    Session::flash('message', $data['nama_metode'] . ' deleted successfully');
	    return redirect('/admin-ds/metodepembayaran');
    }
}
