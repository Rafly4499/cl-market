<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
class CustomerController extends Controller
{
    public function index()
    {
        $customer = Customer::all();
        $user = User::all();
        return view('backend.pages.customer.index', compact('customer','user'));
    }
    public function view($id)
    {
        $customer = Customer::find($id);
        return view('backend.pages.customer.view', compact('customer'));
    }
}
