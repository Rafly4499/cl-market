<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Pemberdayaan;
use Illuminate\Support\Facades\Storage;
use Image;
use File;

class PemberdayaanController extends Controller
{
  public function index() {
	  $data['data'] = Pemberdayaan::orderBy('id','DESC')->get();

      return view('backend.pages.pemberdayaan.index', $data);
  }

  public function create()
  {
      return view('backend.pages.pemberdayaan.create');
  }

  public function store(Request $request)
  {
        $req = $request->all();

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
                })->save($destinationPath.$fileName);
                $req['photo'] = $fileName;
                unset($req['image']);
            }
          }

        $result = Pemberdayaan::create($req);

        Session::flash('message', 'Added successfully');
                return redirect('/admin-ds/pemberdayaan');
  }

    public function edit($id)
    {
      $data['data'] = Pemberdayaan::find($id);

      return view('backend.pages.pemberdayaan.edit', $data);
    }

    public function update($id, Request $request)
    {
          $req = $request->except('_method', '_token', 'submit');
          
          if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'img/'; // upload path
              $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
              $fileName = rand(11111,99999).'.'.$extension; // renaming image
              $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
              Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
              })->save($destinationPath.$fileName);
              $req['photo'] = $fileName;
              unset($req['image']);
    
              $result = Pemberdayaan::find($id);
              if (!empty($result->photo)) {
                File::delete('img/'.$result->photo);
              }
            }else {
              unset($req['photo']);
            }
          }else {
            unset($req['photo']);
          }

        $result = Pemberdayaan::where('id', $id)->update($req);

        Session::flash('message', 'Updated successfully');
        return redirect('admin-ds/pemberdayaan');
    }

    public function destroy($id)
    {
      $result = Pemberdayaan::find($id);
      if (!empty($result->photo)) {
        File::delete('img/'.$result->photo);
      }
      $result->delete();

      Session::flash('message', 'Deleted successfully');
	    return redirect('admin-ds/pemberdayaan');
    }
}
