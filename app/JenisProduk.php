<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisProduk extends Model
{
    protected $table = "tb_jenis_produk";
    protected $fillable= [
        'jenis_produk'
    ];
    public function produk(){
        return $this->hasMany('App\Produk');
    }
    public function kategori_produk(){
        return $this->hasMany('App\KategoriProduk');
    }
}
