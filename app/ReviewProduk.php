<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewProduk extends Model
{
    protected $table = "tb_product_review";
    protected $fillable= [
        'customers_id', 'id_produk','content', 'status'
    ];
    public function customer(){
        return $this->belongsTo('App\Customer','customers_id');
    }
    public function produk(){
        return $this->belongsTo('App\Produk','id_produk');
    }
}
