<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    protected $table = "tb_toko";
    protected $fillable= [
        'id_pedagang', 'nama_toko','alamat_toko', 'deskripsi', 'penilaian', 'photo'
    ];
    public function mitra(){
        return $this->belongsTo('App\Mitra','id_pedagang');
    }
}
