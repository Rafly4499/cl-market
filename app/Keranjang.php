<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = "tb_keranjang";
    protected $fillable= [
        'id_customer', 'id_produk', 'jumlah', 'sub_harga'
    ];
    public function customer(){
        return $this->belongsTo('App\Customer','id_customer');
    }
    public function produk(){
        return $this->belongsTo('App\Produk','id_produk');
    }

}
