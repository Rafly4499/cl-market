<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $table = "tb_penilaian";
    protected $fillable= [
        'id_pedagang', 'id_customer','jumlah_penilaian'
    ];
    public function mitra(){
        return $this->belongsTo('App\Mitra','id_pedagang');
    }
    public function customer(){
        return $this->belongsTo('App\Customer','id_customer');
    }
}
