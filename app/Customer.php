<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Customer extends Authenticatable 
{
    protected $fillable = [
        'username',
        'fullname',
        'email',
        'no_hp',
        'password'
  ];
  protected $hidden = [
       'password', 'remember_token'
  ];
  
  public function setPasswordAttribute($val)
  {
       return $this->attributes['password'] = bcrypt($val);
  }
}   