<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPesanan extends Model
{
    protected $table = "tb_detail_pesanan";
    protected $fillable= [
        'id_pesanan', 'id_produk', 'id_pedagang', 'id_lelang_produk', 'quantity', 'harga_satuan', 'total_subharga'
    ];
    public function pesanan(){
        return $this->belongsTo('App\Pesanan','id_pesanan');
    }
    public function pedagang(){
        return $this->belongsTo('App\Mitra','id_pedagang');
    }
    public function produk(){
        return $this->belongsTo('App\Produk','id_produk');
    }
    public function lelang(){
        return $this->belongsTo('App\LelangProduk','id_lelang_produk');
    }
}
