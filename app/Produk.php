<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = "tb_produk";
    protected $fillable= [
        'id_jenis_produk', 'id_kategori_produk','id_user', 'nama_produk', 'deskripsi', 'satuan', 'stok', 'harga', 'status', 'gambar'
    ];
    public function jenis_produk(){
        return $this->belongsTo('App\JenisProduk','id_jenis_produk');
    }
    public function kategori_produk(){
        return $this->belongsTo('App\KategoriProduk','id_kategori_produk');
    }
    public function user(){
        return $this->belongsTo('App\User','id_user');
    }
}
