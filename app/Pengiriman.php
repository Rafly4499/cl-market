<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    protected $table = "tb_pengiriman";
    protected $fillable= [
        'nama_penerima', 'alamat_pengantaran', 'email', 'no_telpon', 'catatan_order'
    ];
    public function pesanan(){
        return $this->hasMany('App\Pesanan','id_pesanan');
    }
}
