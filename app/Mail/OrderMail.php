<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

   /**
     * The user instance.
     *
     * @var user
     */
    public $info;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function __construct($user)
    {
        $this->info = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this
                 ->subject('Order Tsabitah Food')
                 ->from('tsabitahcoklat@gmail.com')
                 ->view( 'frontend.pages.mails.order', ['info' => $this->info] );
    }
}
