<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('frontend.app');
});


Auth::routes();
Route::group(['prefix' => '/admin-ds', 'middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@adminHome')->name('admin.home')->middleware('auth');
    Route::get('/dashboard', 'DashboardController@index');
    // Route::get('/produk', 'ProdukController@index');
    Route::resource('/produk', 'ProdukController');
    Route::get('/produk', 'ProdukController@index');
    Route::get('/produk/{id}/view', 'ProdukController@view');

    Route::resource('/kategoriproduk', 'KategoriProdukController');
    Route::get('/kategoriproduk', 'KategoriProdukController@index');
    Route::get('/kategoriproduk/create', 'KategoriProdukController@create');
    Route::resource('/review', 'ReviewProdukController');
    Route::get('/review', 'ReviewProdukController@index');
    Route::resource('/jenisproduk', 'JenisProdukController');
    Route::get('/jenisproduk', 'JenisProdukController@index');
    Route::get('/jenisproduk/create', 'JenisProdukController@create');
    Route::resource('/admin', 'AdminController');
    Route::get('/admin', 'AdminController@index');
    Route::resource('/customer', 'CustomerController');
    Route::get('/customer', 'CustomerController@index');
    Route::get('/customer/{id}/view', 'CustomerController@view');
    Route::resource('/mitra', 'MitraController');
    Route::get('/mitra', 'MitraController@index');
    Route::get('/mitra/{id}/view', 'MitraController@view');
    Route::resource('/toko', 'TokoController');
    Route::get('/toko', 'TokoController@index');
    Route::get('/toko/{id}/view', 'TokoController@view');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::resource('/metodepembayaran', 'MetodePembayaranController');
    Route::get('/metodepembayaran', 'MetodePembayaranController@index');
    Route::get('/metodepembayaran/{id}/view', 'MetodePembayaranController@view');

    Route::resource('/pesanan', 'PesananController');
    Route::get('/pesanan', 'PesananController@index');
    Route::get('/pesanan/{id}/view', 'PesananController@view');
    Route::get('/pesanan/{id}/rincianpesanan', 'PesananController@rincian_pesanan');
    Route::get('/pesanan/{id}/detailpayment', 'PesananController@detailpayment');
    Route::get('/pesanan/approval/{id}', 'PesananController@approval');
    Route::get('/pesanan/rejected/{id}', 'PesananController@rejected');
});


//Frontend

Route::get('/','Frontend\LandingPageController@index');
Route::get('/category/{id}','Frontend\LandingPageController@category');
Route::get('/users/login', 'Frontend\CustomerController@showLoginForm')->name('customer.loginform');
Route::get('/users/register', 'Frontend\CustomerController@showRegisterForm')->name('customer.registerform');
Route::post('/users/login', 'Frontend\CustomerController@login')->name('customer.login');
Route::post('/customer/register', 'Frontend\CustomerController@register')->name('customer.register');
Route::get('/jenis-product/{id}','Frontend\ServicesController@jenisproduk')->name('jenisshow');
Route::get('/category-product/{id}','Frontend\ServicesController@kategoriproduk')->name('kategoriproduk.show');
Route::post('/sorting', 'Frontend\ServicesController@sorting')->name('sorting');
Route::get( '/searchproduct', 'Frontend\ServicesController@searchProduct' )->name('searchproduct');
Route::get( '/shop', 'Frontend\ServicesController@shop' )->name('shop');
Route::get( '/about', 'Frontend\AboutUsController@index' )->name('about');
Route::get( '/detailproduct/{id}', 'Frontend\ServicesController@detailProduk' )->name('detailproduct');

Route::group(['prefix' => '/users', 'middleware' => ['auth:customer']], function() {
    Route::get('/', 'Frontend\CustomerController@index')->middleware('auth:customer');
    Route::post('/edit-data/{id}', 'Frontend\ServicesController@updateCustomer')->name('customer.update');
    Route::post('/product/{id}', 'Frontend\LandingPageController@product');
    // Route::get('/pesanan-selesai/{id}', 'Frontend\ServicesController@pesananSelesai');
    // Route::get('/customeredit/{id}', 'Frontend\ServicesController@editData');
    // Route::get('/logout', 'Frontend\CustomerController@logout')->name('customer.logout');
    // Route::get('/confirmation-payment/{id}', 'Frontend\ServicesController@viewconfirmationPayment');
    // Route::post('/confirmation-payment', 'Frontend\ServicesController@PostConfirmationPayment')->name('confirmation');
    // Route::post('/review-produk', 'Frontend\ServicesController@reviewProduk')->name('review');
    // Route::get('/invoice/{id}', 'Frontend\ServicesController@viewinvoice');
    // Route::get('/cart/{id}', 'Frontend\ServicesController@cart');
    // Route::post('/submitCart', 'Frontend\ServicesController@submitCart')->name('cart');
    // Route::get('/checkout/{id}', 'Frontend\ServicesController@checkout');
    // Route::post('/checkout', 'Frontend\ServicesController@viewinvoice');
    // Route::post('/delete-keranjang/{id}','Frontend\ServicesController@deleteCart');
    // Route::post('/submitCheckout', 'Frontend\ServicesController@submitCheckout');
    // Route::post('/submitOrder', 'Frontend\ServicesController@submitOrder')->name('submitOrder');
});