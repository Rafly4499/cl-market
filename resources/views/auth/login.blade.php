@extends('layouts.app')

@section('content')
                    <div class="mainlogin">
                        <img src="{{ asset('img/tsabitahfood.png') }}" style="width: 200px;" alt="">
                        <span class="login100-form-title p-t-33 p-b-33">
                            Admin Login
                        </span>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="wrap-input100">
                    

                                <input id="email" type="email"  placeholder="Masukkan Email Anda"  class="form-control input100 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="wrap-input100">
                                <input id="password" type="password" placeholder="Masukkan Password Anda" class="form-control input100 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                            <div class="container-login100-form-btn m-t-20">
                                <button type="submit" class="login100-form-btn">
                                    {{ __('Login') }}
                                </button>

                                {{-- @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif --}}
                                @if (Route::has('login'))
                                <div class="text-center">
                                    <span class="txt1">
                                        Do You Not have an account?
                                    </span>

                                    <a href="{{ route('register') }}" class="txt2 hov1">
                                        Sign up
                                    </a>
                                </div>
                                @endif
                            </div>
                        
                    </form>
                </div>
@endsection
