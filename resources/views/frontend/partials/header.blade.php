<header class="section-header">
    <section class="header-main shadow-sm bg-white">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-1">
                    <a href="home.html" class="brand-wrap mb-0">
                        <img alt="#" class="img-fluid" src="{{ asset('frontend/img/logo_web.png') }}">
                    </a>

                </div>
                <div class="col-3 d-flex align-items-center m-none">

                </div>

                <div class="col-8">
                    <div class="d-flex align-items-center justify-content-end pr-5">
                        <div style="float: right; cursor: pointer;">
                            <span class="glyphicon glyphicon-shopping-cart my-cart-icon"><span class="badge badge-notify my-cart-badge"></span></span>
                          </div>
                        <div class="dropdown mr-4 m-none">
                            <a href="#" class="dropdown-toggle text-dark py-3 d-block" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                            </a>
                            
                        </div>
                        <a class="toggle" href="#">
                            <span></span>
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </section>

</header>