@extends('frontend.app') 
@section('pageTitle', 'Customer')
@section('content')
<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>My Account</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                    <li class="breadcrumb-item active">My Account</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Wishlist  -->
<div class="wishlist-box-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="product-categori">
                    <div class="filter-sidebar-left">
                        <div class="title-left">
                            <h3 class="juduldata">Data Diri</h3>
                        </div>
                        <div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
                            <div class="list-group-collapse sub-men">
                                Username
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->username }}</p>
                            </div>
                            <div class="list-group-collapse sub-men">
                                Nama Lengkap 
                            
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->fullname }}</p>
                            </div>
                            
                            <div class="list-group-collapse sub-men">
                                Email
                            
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->email }}</p>
                            </div>
                            <div class="list-group-collapse sub-men">
                                No HP
                            
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->no_hp }}</p>
                            </div>

                        </div>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-6">
                                <a href="{{ url('customer/customeredit/'.Auth::guard('customer')->user()->id) }}" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Update</a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="btn btn-danger d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                            
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-9">
                <form action="{{ url('/customer/edit-data/'.Auth::guard('customer')->user()->id) }}" method="POST" class="js-validate" enctype="multipart/form-data">
                    @csrf
                <div class="col-sm-12 mb-4">
                    <div class="js-form-message">
                        <label class="form-label">
                            Username
                        </label>
                        <input type="text" class="form-control" name="username" value="{{ $account->username }}">
                    </div>
                </div>
                <!-- End Input -->

                <!-- Input -->
                <div class="col-sm-12 mb-4">
                    <div class="js-form-message">
                        <label class="form-label">
                            Nama Lengkap
                        </label>
                        <input type="text" class="form-control" name="fullname" value="{{ $account->fullname }}">
                    </div>
                </div>
                <div class="col-sm-12 mb-4">
                    <div class="js-form-message">
                        <label class="form-label">
                            Email Address
                        </label>

                        <input type="email" class="form-control" name="email" value="{{ $account->email }}">
                    </div>
                </div>
                <!-- End Input -->
                <div class="col-sm-12 mb-4">
                    <div class="js-form-message">
                        <label class="form-label">
                            No HP
                        </label>

                        <input type="number" class="form-control" name="no_hp" value="{{ $account->no_hp }}">
                    </div>
                </div>
                <div class="w-100"></div>
                            <!-- End Input -->

                            <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-wide w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">Update Data</button>
                            </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection