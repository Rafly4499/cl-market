@extends('frontend.app') 
@section('pageTitle', 'Customer')
@section('content')
 <!-- Start All Title Box -->
 <div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>My Account</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Beranda</a></li>
                    <li class="breadcrumb-item active">My Account</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Wishlist  -->
<div class="wishlist-box-main">
    <div class="container">
        <div class="row">
            @if ($message = Session::get('success'))
            <script>
                Swal.fire({
                    title: 'Berhasil!',
                    text:  '<?php echo $message; ?>',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                  })
                </script>
            @endif
            @if ($message = Session::get('error'))
            <script>
                Swal.fire({
                    title: 'Gagal!',
                    text:  '<?php echo $message; ?>',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                  })
                </script>
            @endif
            <div class="col-lg-3">
                <div class="product-categori">
                    <div class="filter-sidebar-left">
                        <div class="title-left">
                            <h3 class="juduldata">Data Diri</h3>
                        </div>
                        <div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
                            <div class="list-group-collapse sub-men">
                                Username
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->username }}</p>
                            </div>
                            <div class="list-group-collapse sub-men">
                                Nama Lengkap 
                            
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->fullname }}</p>
                            </div>
                            
                            <div class="list-group-collapse sub-men">
                                Email
                            
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->email }}</p>
                            </div>
                            <div class="list-group-collapse sub-men">
                                No HP
                            
                            </div>
                            <div class="list-group-collapse sub-men">
                                <p class="isidata">{{ Auth::guard('customer')->user()->no_hp }}</p>
                            </div>

                        </div>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-md-6">
                                <a href="{{ url('customer/customeredit/'.Auth::guard('customer')->user()->id) }}" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Update</a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="btn btn-danger d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                            
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-9">
                <div class="table-main table-responsive">
                    
                    <div class="title-left">
                        <h3 class="juduldata">Riwayat Order</h3>
                    </div>
                    @if(count($order) == 0)
                    <div class="text-center">
                        
                        <h1>Belum ada data Order, silahkan melakukan order anda</h1>
                        <a href="{{ url('/shop') }}" style="width:200px !important;margin: 0 auto;" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Cari produk</a>
                    </div>    
                        @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>OrderID</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Pembayaran</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order as $item)
                            <tr>
                                <td>#{{ $item->no_order }}</td>
                                <td>{{ date('d M Y', strtotime($item->date_order)) }}</td>
                                <td>{{ $item->status }}</td>
                                <td>Rp. {{ number_format($item->total_harga, 0) }},-</td>
                                <td><a href="{{ url('/customer/invoice/'.$item->id) }}" class="btn btn-primary btn-sm btn-block flex-horizontal-center text-lh-sm mb-2">
                                    <i class="fas fa-receipt"></i> Invoice Order
                                  </a>
                                  @if($item->status == 'Sudah Bayar')
                                  <a href="{{ url('/customer/pesanan-selesai/'.$item->id) }}" onclick="return confirm('Apakah anda yakin, pesanan anda sudah sampai?')" class="btn btn-success btn-sm btn-block flex-horizontal-center text-lh-sm mb-2"><i class="fas fa-clipboard-check"></i> Pesanan Sampai</a>
                                  @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Wishlist -->
@endsection