<?php
    use App\DetailPesanan;
?>
<html>

<body style="background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;">
  <table style="max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px red;">
    <thead>
      <tr>
        <th style="text-align:left;"><img style="max-width: 150px;" src="{{ asset('img/tsabitahfood.png') }}" alt="bachana tours"></th>
        <th style="text-align:right;font-weight:400;">{{  date('d M Y H:i:s', strtotime($info['date_order'])) }}</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="height:35px;"></td>
      </tr>
      <tr>
        <td colspan="2" style="border: solid 1px #ddd; padding:10px 20px;">
          <p style="font-size:14px;margin:0 0 6px 0;"><span style="font-weight:bold;display:inline-block;min-width:150px">Order status</span><b style="color:red;font-weight:normal;margin:0">{{ $info['status']}}</b></p>
          <p style="font-size:14px;margin:0 0 6px 0;"><span style="font-weight:bold;display:inline-block;min-width:146px">Transaction ID</span> {{ $info['no_order']}}</p>
          <p style="font-size:14px;margin:0 0 0 0;"><span style="font-weight:bold;display:inline-block;min-width:146px">Order amount</span> Rp. {{ number_format($info['total_harga'], 0) }}</p>
        </td>
      </tr>
      <tr>
        <td style="height:35px;"></td>
      </tr>
      <tr>
        <td style="width:50%;padding:20px;vertical-align:top">
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px">Nama Lengkap</span> {{ $info->customer['fullname']}}</p>
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Email</span> {{ $info->customer['email']}}</p>
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Phone</span> {{ $info->customer['no_hp']}}</p>
          
        </td>
        <td style="width:50%;padding:20px;vertical-align:top">
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Alamat Pengiriman</span> {{ $info['alamat_pengiriman']}}</p>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="font-size:20px;padding:30px 15px 0 15px;">Items</td>
      </tr>
      <tr>
        <td colspan="2" style="padding:15px;">
            <?php
                $detpesanan = DetailPesanan::where('id_pesanan',$info->id)->get();
            ?>
            @foreach($detpesanan as $item)
          <p style="font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;">
            <span style="display:block;font-size:13px;font-weight:normal;">{{ $item->produk['nama_produk']}}</span> Rp. {{ number_format($item->total_subharga, 0) }}<b style="font-size:12px;font-weight:300;"> | QTY : {{ $item->quantity}}</b>
          </p>
          @endforeach
        </td>
      </tr>
    </tbody>
    <tfooter>
      <tr>
        <td colspan="2" style="font-size:14px;padding:50px 15px 0 15px;">
          <div class="w-100"></div>
          <div class="pt-4 border-bottom">
              <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-3">
                  Payment
              </h5>
              <img class="img-fluid" style="width:150px;" src="https://bankmandiri.co.id/image/layout_set_logo?img_id=31567&t=1582304184496" alt="" />
              <p class="">
                  Silahkan segera melakukan pembayaran ke Rekening <br><b>Bank Mandiri 1400015860704 An.ALI SUBAGYO.</b>
              </p>
              <p class="">
                Jika sudah melakukan pembayaran silahkan konfirmasi pembayaran tersebut melalui halaman invoice pada link website , pilih invoice order anda.
            </p>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="font-size:14px;padding:50px 15px 0 15px;">
          <strong style="display:block;margin:0 0 10px 0;">Hormat kami</strong> Tsabitah Food<br> Indonesia<br><br>
          <b>Phone:</b> +62 8965-1632-799<br>
          <b>Email:</b> tsabitahcoklat@gmail.com
        </td>
      </tr>
    </tfooter>
  </table>
</body>
</html>