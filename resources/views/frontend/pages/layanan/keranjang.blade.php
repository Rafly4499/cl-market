@extends('frontend.app') 
@section('pageTitle', 'Keranjang')
@section('content')

<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Keranjang</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item active">Keranjang</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        
        <div class="row">
            
            <div class="col-lg-12">
                @if(count($cart) == 0)
                    <div class="text-center">
                        <h1>Belum ada data produk di keranjang, silahkan menambahkan produk</h1>
                        <a href="{{ url('/shop') }}" style="width:200px !important;margin: 0 auto;" class="btn btn-primary d-flex align-items-center justify-content-center height-60 w-100 mb-xl-0 mb-lg-1 transition-3d-hover font-weight-bold">Cari produk</a>
                    </div>    
                        @else
                        <h1>Keranjang Produk</h1>
                        <p>Saat ini Anda memiliki {{ count($cart) }} produk di keranjang Anda</p> <br>
                <div class="table-main table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Gambar</th>
                                <th>Nama Produk</th>
                                <th>Harga</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody> 
                        @foreach ($cart as $item)
                            <tr>
                                <td class="thumbnail-img">
                                <img class="img-fluid" src="{{ url('img/'.$item->produk['gambar']) }}" alt="" />
                                </td>
                                <td class="name-pr">
                                {{ $item->produk['nama_produk']}}
                                </td>
                                <td class="price-pr">
                                    <p>Rp. {{ number_format($item->produk['harga'], 0) }}</p>
                                </td>
                                <td class="quantity-box">
                                    <p>{{ $item->jumlah}} {{ $item->produk['satuan']}}</p>
                                </td>
                                <td class="total-pr">
                                    <p>Rp. {{ number_format($item->sub_harga, 0) }}</p>
                                </td>
                                <td class="remove-pr">
                                    <form action="{{ url('/customer/delete-keranjang/'.$item->id) }}" method="post">
                                        {{ csrf_field() }}
                                        <button class="delkeranjang"type="submit" onclick="return confirm('Are you sure?')"><i class="fas fa-times"></i> </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
        @if(count($cart) > 0)
            <div class="row justify-content-end">
                <div class="col-lg-4 col-sm-12">
                <div class="col-6 d-flex shopping-box"><a href="/shop" class="btn btn-primary">Tambah Produk</a> </div>
                </div>
            <div class="col-lg-4 col-sm-12">
                <div class="order-box">
                    <div class="d-flex gr-total">
                        <h5>Grand Total</h5>
                    <div class="ml-auto h5">Rp. {{ number_format($cart->sum('sub_harga'), 0) }}</div>
                    </div>
                    <hr> </div>
                    
                <div class="col-12 d-flex shopping-box justify-content-end">
                    <form method="POST" action="{{url('/customer/submitCheckout')}}">
                        @csrf
                        <input type="hidden" name="subtotal_harga" value="{{$cart->sum('sub_harga')}}">
                        <input type="hidden" name="total_pembayaran" value="{{$cart->sum('sub_harga')}}">
                    <button type="submit" class="ml-auto btn hvr-hover">Checkout</button> 
                    </form>
            </div>
            </div>
            </div>
            @endif
    </div>
</div>
<div class="shop-detail-box-main">
    <div class="container">
<div class="row my-2">
    <div class="col-lg-12">
            <h1>Anda mungkin juga menyukai produk ini</h1>
        <div class="product-categorie-box">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade show active">
                    <div class="row">
                        @foreach($lainnya as $item)
                        <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                            <div class="products-single fix">
                                <div class="box-img-hover">
                                    <div class="type-lb">
                                        @if($item->stok > 0)
                                        <p class="sale">Sale</p>
                                        @else
                                        <p class="sale">Out Of Stock</p>
                                        @endif
                                    </div>
                                    <img src="{{ url('img/'.$item->gambar) }}" class="img-fluid" alt="Image">
                                    <div class="mask-icon">
                                        <ul>
                                            <li><a href="{{ url('detailproduct/'.$item->id) }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="View"><i class="fas fa-eye"></i></a></li>
                                        </ul>
                                        @if (auth()->guard('customer')->check())
                                            <a class="cart" href="{{url('/customer/cart/'. Auth::guard('customer')->user()->id )}}">Add to Cart</a>
                                            @else
                                            <a class="cart" onclick="reqLogin()" href="{{url('/customer/login')}}">Add to Cart</a>
                                            <script>
                                            function reqLogin() {
                                            alert("Anda harus melakukan login terlebih dahulu");
                                            }
                                            </script>
                                            @endif
                                    </div>
                                </div>
                                <div class="why-text">
                                <h4>{{ $item->nama_produk }}</h4>
                                    <h5> Rp. {{ number_format($item->harga, 0) }}/{{ $item->satuan }}</h5>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</div>
<!-- End Cart -->
@endsection