@extends('frontend.app') 
@section('pageTitle', 'Invoice')
@section('content')
<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Invoice</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item active">Invoice</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        
            <div class="col-sm-12 col-lg-12 mb-3">
            <div class="row">
                @if ($message = Session::get('success'))
                <script>
                    Swal.fire({
                        title: 'Berhasil!',
                        text:  '<?php echo $message; ?>',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                @if ($message = Session::get('error'))
                <script>
                    Swal.fire({
                        title: 'Gagal!',
                        text:  '<?php echo $message; ?>',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="checkout-address">
                    <div class="title-left">
                        <h1>Data Invoice</h1>
                    </div>
                    
                        <input type="hidden" name="id_customer" value="{{ Auth::guard('customer')->user()->id }}">
                            <div class="d-flex">
                                <h4>Nama Lengkap</h4>
                                <div class="ml-auto font-weight-bold"> {{ Auth::guard('customer')->user()->fullname }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Username</h4>
                                <div class="ml-auto font-weight-bold"> {{ Auth::guard('customer')->user()->username }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Email Address</h4>
                                <div class="ml-auto font-weight-bold"> {{ Auth::guard('customer')->user()->email }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>No HP</h4>
                                <div class="ml-auto font-weight-bold"> {{ Auth::guard('customer')->user()->no_hp }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Alamat Pengiriman</h4>
                                <div class="ml-auto font-weight-bold"> {{ $pesanan->alamat_pengiriman }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Catatan Order</h4>
                                <div class="ml-auto font-weight-bold">{{ $pesanan->catatan_order }}</div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Status</h4>
                                <div class="ml-auto font-weight-bold"> {{ $pesanan->status }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Tanggal Order</h4>
                                <div class="ml-auto font-weight-bold"> {{  date('d M Y H:i:s', strtotime($pesanan->date_order)) }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Batas Waktu Pembayaran</h4>
                                <div class="ml-auto font-weight-bold"> {{  date('d M Y H:i:s', strtotime($pesanan->date_limit)) }} </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Tanggal Bayar</h4>
                                <div class="ml-auto font-weight-bold"> @if($pesanan->date_payment)
                                    {{  date('d M Y H:i:s', strtotime($pesanan->date_payment)) }}
                                    @else
                                    Belum melakukan pembayaran
                                    @endif </div>
                            </div>
                            <hr>
                            <div class="d-flex">
                                <h4>Tanggal Pesanan selesai</h4>
                                <div class="ml-auto font-weight-bold"> @if($pesanan->date_done)
                                    {{  date('d M Y H:i:s', strtotime($pesanan->date_done)) }}
                                    @else
                                    Masih belum selesai
                                    @endif</div>
                            </div>
                            <hr>

                        <hr class="mb-1"> 
                    
                </div>
            </div>
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="odr-box">
                            <div class="title-left">
                            <h3>No Order : {{ $pesanan->no_order }}</h3>
                            </div>
                            @foreach($detpesanan as $item)
                            <div class="rounded p-2 bg-light">
                                <div class="media mb-2 border-bottom">
                                    <div class="col-md-3">
                                        <div class="thumbnail-img">
                                            <img class="img-fluid" src="{{ url('img/'.$item->produk['gambar']) }}" alt="" />
                                            </div>
                                    </div>
                                    <div class="col-md-9">
                                    <div class="media-body"> <a href="{{ url('detailproduct/'.$item->id) }}"> {{ $item->produk['nama_produk']}}</a>
                                            <div class="small text-muted">Price: Rp. {{ number_format($item->harga_satuan, 0) }} <span class="mx-2">|</span> Qty: {{ $item->quantity}} <span class="mx-2">|</span> Subtotal: Rp. {{ number_format($item->total_subharga, 0) }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="order-box">
                            <div class="title-left">
                                <h3>Your order</h3>
                            </div>
                            <div class="d-flex">
                                <div class="font-weight-bold">Product</div>
                                <div class="ml-auto font-weight-bold">Total</div>
                            </div>
                            <hr class="my-1">
                            <div class="d-flex">
                                <h4>Sub Total</h4>
                                <div class="ml-auto font-weight-bold"> Rp. {{ number_format($pesanan->total_harga, 0) }} </div>
                            </div>
                            <hr>
                            <div class="d-flex gr-total">
                                <h5>Grand Total</h5>
                                <div class="ml-auto h5"> Rp. {{ number_format($pesanan->total_harga, 0) }} </div>
                            </div>
                            <hr> </div>
                            <div class="w-100"></div>
                            <div class="pt-4 border-bottom">
                                <h5 id="scroll-description" class="font-size-21 font-weight-bold text-dark mb-3">
                                    Payment
                                </h5>
                                <img class="img-fluid" style="width:200px;" src="{{ asset('frontend/assets/images/desaloka/mandiri.png') }}" alt="" />
                                <p class="">
                                    Silahkan segera melakukan pembayaran ke Rekening <br><b>Bank Mandiri 1400017056343 An.RAFLY ARIEF KANZA.</b>
                                </p>
                            </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                    <div class="pt-4">
                        <div class="row justify-content-end">
                        <div class="col-sm-6 mb-4">
                            <a href="/customer"><button class="btn btn-primary w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">My Account</button></a>
                        </div>
                        @if($pesanan->status != 'Sudah Bayar')
                        <div class="col-sm-6 mb-4">
                            <a href="{{ url('customer/confirmation-payment/'.$pesanan->id) }}"><button type="submit" style="color:white;" class="w-100 rounded-sm transition-3d-hover font-weight-bold py-3 btn hvr-hover">Confirmation Payment</button>
                        </div>
                        @endif
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
        

    </div>
</div>
<!-- End Cart -->


@endsection