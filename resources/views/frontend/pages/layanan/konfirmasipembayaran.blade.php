@extends('frontend.app') 
@section('pageTitle', 'Konfirmasi Pembayaran')
@section('content')

<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Confirmation Payment</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item active">Confirmation Payment</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        
            <div class="col-sm-12 col-lg-12 mb-3">
            <div class="row">
                @if ($message = Session::get('success'))
                <script>
                    Swal.fire({
                        title: 'Berhasil!',
                        text:  '<?php echo $message; ?>',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                @if ($message = Session::get('error'))
                <script>
                    Swal.fire({
                        title: 'Gagal!',
                        text:  '<?php echo $message; ?>',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="checkout-address">
                    <div class="title-left">
                        <h3>Confirmation Payment</h3>
                    </div>
                <form action="{{ route('confirmation')}}" method="POST" class="js-validate" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <!-- Input -->
                            <div class="col-sm-12 mb-4">
                                <div class="js-form-message">
                                    <label class="form-label">
                                        ID Order
                                    </label>
                                    <input type="text" class="form-control" name="invoiceid" value="{{ $pesanan->no_order }}" disabled>
                                    <input type="hidden" class="form-control" name="invoiceid" value="{{ $pesanan->no_order }}">
                                </div>
                            </div>
                            <!-- End Input -->

                            <!-- Input -->
                            <div class="col-sm-12 mb-4">
                                <div class="js-form-message">
                                    <label class="form-label">
                                        Tujuan Transfer
                                    </label>
                                    <input type="text" class="form-control" name="transfer_destination" placeholder="Masukkan tujuan tranfer" aria-label="Masukkan tujuan tranfer" required
                                    data-msg="Masukkan tujuan tranfer"
                                    data-error-class="u-has-error"
                                    data-success-class="u-has-success">
                                </div>
                            </div>
                            <div class="col-sm-12 mb-4">
                                <div class="js-form-message">
                                    <label class="form-label">
                                        Pemilik Rekening
                                    </label>

                                    <input type="text" class="form-control" name="account_owner" placeholder="Masukkan pemilik rekening" aria-label="Ali" required
                                    data-msg="Masukkan pemilik rekening."
                                    data-error-class="u-has-error"
                                    data-success-class="u-has-success">
                                </div>
                            </div>
                            <!-- End Input -->
                            <div class="col-sm-12 mb-4">
                                <div class="js-form-message">
                                    <label class="form-label">
                                        Tanggal Pembayaran
                                    </label>

                                    <input type="date" class="form-control" name="date_payment" aria-label="date_payment" required
                                    data-msg="Masukkan tanggal pembayaran"
                                    data-error-class="u-has-error"
                                    data-success-class="u-has-success">
                                </div>
                            </div>
                            <!-- Input -->
                            <div class="col-sm-12 mb-4">
                                <div class="js-form-message">
                                    <label class="form-label">
                                        Total Pembayaran
                                    </label>

                                    <input type="number" class="form-control" name="total_payment" placeholder="Masukkan total pembayaran" aria-label="TUFAN" required
                                    data-msg="Masukkan total pembayaran."
                                    data-error-class="u-has-error"
                                    data-success-class="u-has-success">
                                </div>
                            </div>
                            <div class="col-sm-12 mb-4">
                                <div class="js-form-message">
                                    <label class="form-label">
                                        No Handphone
                                    </label>

                                    <input type="number" class="form-control" name="no_hp" placeholder="Masukkan no handphone yang bisa dihubungi" aria-label="TUFAN" required
                                    data-msg="Masukkan no handphone yang bisa dihubungi."
                                    data-error-class="u-has-error"
                                    data-success-class="u-has-success">
                                </div>
                            </div>
                            <div class="col-sm-12 mb-4">
                               
                                    <label class="form-label">
                                        Bukti Pembayaran
                                    </label>

                                    <input type="file" name="image" id="preview" class="image" required>

                            </div>
                            <!-- End Input -->

                            <!-- Input -->
                            
                            <!-- End Input -->

                            <div class="w-100"></div>

                            <!-- Input -->
                            
                            <!-- End Input -->

                            <div class="w-100"></div>
                            <!-- End Input -->

                            <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-wide w-100 rounded-sm transition-3d-hover font-size-16 font-weight-bold py-3">SUBMIT</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="odr-box">
                            <div class="title-left">
                            <h3>No Order : {{ $pesanan->no_order }}</h3>
                            </div>
                            @foreach($detpesanan as $item)
                            <div class="rounded p-2 bg-light">
                                <div class="media mb-2 border-bottom">
                                    <div class="col-md-3">
                                        <div class="thumbnail-img">
                                            <img class="img-fluid" src="{{ url('img/'.$item->produk['gambar']) }}" alt="" />
                                            </div>
                                    </div>
                                    <div class="col-md-9">
                                    <div class="media-body"> <a href="{{ url('detailproduct/'.$item->id) }}"> {{ $item->produk['nama_produk']}}</a>
                                            <div class="small text-muted">Price: Rp. {{ number_format($item->harga_satuan, 0) }} <span class="mx-2">|</span> Qty: {{ $item->quantity}} <span class="mx-2">|</span> Subtotal: Rp. {{ number_format($item->total_subharga, 0) }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="order-box">
                            <div class="title-left">
                                <h3>Your order</h3>
                            </div>
                            <div class="d-flex">
                                <div class="font-weight-bold">Product</div>
                                <div class="ml-auto font-weight-bold">Total</div>
                            </div>
                            <hr class="my-1">
                            <div class="d-flex">
                                <h4>Sub Total</h4>
                                <div class="ml-auto font-weight-bold"> Rp. {{ number_format($pesanan->total_harga, 0) }} </div>
                            </div>
                            <hr>
                            <div class="d-flex gr-total">
                                <h5>Grand Total</h5>
                                <div class="ml-auto h5"> Rp. {{ number_format($pesanan->total_harga, 0) }} </div>
                            </div>
                            <hr> </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
        

    </div>
</div>
<!-- End Cart -->


@endsection