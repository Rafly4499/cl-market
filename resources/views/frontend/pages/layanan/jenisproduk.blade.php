@extends('frontend.app') 
@section('pageTitle', 'Shopping')
@section('content')
<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Shop</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                    <li class="breadcrumb-item active">Shop</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Shop Page  -->
<div class="shop-box-inner">
    <div class="container">
        <div class="row">
            @if ($message = Session::get('success'))
            <script>
                Swal.fire({
                    title: 'Berhasil!',
                    text:  '<?php echo $message; ?>',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                  })
                </script>
            @endif
            @if ($message = Session::get('error'))
            <script>
                Swal.fire({
                    title: 'Gagal!',
                    text:  '<?php echo $message; ?>',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                  })
                </script>
            @endif
            <div class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 shop-content-right">
                <div class="right-product-box">
                    <div class="product-item-filter row">
                        <div class="col-12 col-sm-8 text-center text-sm-left">
                            <div class="toolbar-sorter-right">
                                <span>Sort by </span>
                            <form class="formsort" action="{{ url('sorting')}}" method="POST">
                                @csrf
                                <select id="basic" name="sort" class="selectpicker show-tick form-control">
                                    <option data-display="Select">Nothing</option>
                                    <option value="Produk Terbaru">New Product</option>
                                    <option value="Produk Termahal">High Price → Low Price</option>
                                    <option value="Produk Termurah">Low Price → High Price</option>
                                </select>
                            <button class="btn hvr-hover btnsort" type="submit">Filter</button>
                            </form>
                            </div>
                            <p>Showing all {{ $countproduk }} results</p>
                        </div>
                    </div>

                    <div class="product-categorie-box">
                        <div class="tab-content">
                            @if($product->count() == 0)
                            <h3 class="title">Hasil pencarian pada kategori: "{{ $jenis->jenis_produk }}"</h3> 
                            <h4>Pencarian Anda Tidak Ditemukan</h4>
                            @else
                            <h3 class="title">Hasil pencarian pada kategori: "{{ $jenis->jenis_produk }}"</h3> <br>
                            @endif
                            <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                                <div class="row">

                                    @foreach($product as $item)
                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                        <div class="products-single fix">
                                            <div class="box-img-hover">
                                                <div class="type-lb">
                                                    @if($item->stok > 0)
                                                    <p class="sale">Sale</p>
                                                    @else
                                                    <p class="sale">Out Of Stock</p>
                                                    @endif
                                                </div>
                                                <img src="{{ url('img/'.$item->gambar) }}" class="img-fluid" alt="Image">
                                                <div class="mask-icon">
                                                    <ul>
                                                        <li><a href="{{ url('detailproduct/'.$item->id) }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="Detail Produk"><i class="fas fa-eye"></i></a></li>
                                                    </ul>
                                                    @if (auth()->guard('customer')->check())
                                                    <form method="POST" action="{{ route('cart')}}">
                                                        @csrf
                                                        <input type="hidden" name="id_produk" value="{{$item->id}}">
                                                        <input type="hidden" name="sub_harga" value="{{$item->harga}}">
                                                        <input type="hidden" name="jumlah" value="1">
                                                    <button class="cart" type="submit">Add to Cart</button>
                                                    </form>
                                                    @else
                                                    <a class="cart" onclick="reqLogin()" href="{{url('/customer/login')}}">Add to Cart</a>
                                                    <script>
                                                    function reqLogin() {
                                                    alert("Anda harus melakukan login terlebih dahulu");
                                                    }
                                                    </script>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="why-text">
                                            <h4>{{ $item->nama_produk }}</h4>
                                                <h5> Rp. {{ number_format($item->harga, 0) }}/{{ $item->satuan }}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                
                                
                            </div>
                        </div>
                        <div class="search_left" style="clear:both;">
                            <ul class='page-numbers'>
                            <li>{!! $product->links() !!}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
                <div class="product-categori">
                    <div class="search-product">
                        <form action="/searchproduct" method="GET" class="search-form">
                            @csrf
                            <input type="text" name="search" class="form-control" placeholder="Cari Produk" value="{{ old('search') }}">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    <div class="filter-sidebar-left">
                        <div class="title-left">
                            <h3>Categories</h3>
                        </div>
                        <div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men" data-children=".sub-men">
                            @foreach($jenisItems as $data)
                            <div class="list-group-collapse sub-men">
                                <a class="list-group-item list-group-item-action" href="{{ route('jenisshow', $data->id) }}">{{ $data->jenis_produk }}
                            </a>
                                <div class="collapse show" id="sub-men1" data-parent="#list-group-men">
                                    <div class="list-group">
                                        @if($data->kategori_produk->count()) 
                                        @foreach($data->kategori_produk as $res)
                                        <li><a href="{{ route('kategoriproduk.show', $res->id) }}" class="list-group-item list-group-item-action active">{{ $res->kategori_produk }}</a></li>
                                        @endforeach 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Shop Page -->
@endsection