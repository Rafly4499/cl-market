@extends('frontend.app') 
@section('pageTitle', 'Detail Produk')
@section('content')
 <!-- Start All Title Box -->
 <div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Shop Detail</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/shop">Shop</a></li>
                    <li class="breadcrumb-item active">Shop Detail </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Shop Detail  -->
<div class="shop-detail-box-main">
    <div class="container">
        <div class="row">
            @if ($message = Session::get('success'))
            <script>
                Swal.fire({
                    title: 'Berhasil!',
                    text:  '<?php echo $message; ?>',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                  })
                </script>
            @endif
            @if ($message = Session::get('error'))
            <script>
                Swal.fire({
                    title: 'Gagal!',
                    text:  '<?php echo $message; ?>',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                  })
                </script>
            @endif
            <div class="col-xl-5 col-lg-5 col-md-6">
                <div id="carousel-example-1" class="single-product-slider carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active"> <img class="d-block w-100" src="{{ url('img/'.$product->gambar) }}" alt="First slide"> </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-6">
                <div class="single-product-details">
                <h2>{{ $product->nama_produk}}</h2>
                    <h5>Rp. {{ number_format($product->harga, 0) }}/{{ $product->satuan }}</h5>
                    <p class="available-stock"><span> Stok Produk : <a href="#"> {{ $product->stok}} {{ $product->satuan }}</a></span><p>
                    <p class="available-stock"><span> Komoditas : <a href="#"> {{ $product->jenis_produk['jenis_produk']}}</a></span><p>
                    <p class="available-stock"><span> Kategori Produk : <a href="#"> {{ $product->kategori_produk['kategori_produk']}}</a></span><p>
                    <h4>Short Description:</h4>
                    <p>{!! $product->deskripsi !!}</p>
                    <form method="POST" action="{{ route('cart')}}">
                        @csrf
                    <input type="hidden" name="id_produk" value="{{$product->id}}">
                    <input type="hidden" name="sub_harga" value="{{$product->harga}}">
                    <ul>
                        <li>
                            <div class="form-group quantity-box">
                                <label class="control-label">Quantity</label>
                                <input class="form-control" name="jumlah" placeholder="0" min="0" max="{{ $product->stok}}" type="number" required>
                            </div>
                        </li>
                    </ul>

                    <div class="price-box-bar">
                        <div class="cart-and-bay-btn">
                            @if($product->stok > 0)
                            <button class="btn hvr-hover" style="color:white;" type="submit">Beli Produk</button>
                            @else
                            <a class="btn hvr-hover">Stok Habis</a>
                            @endif
                            <a class="btn hvr-hover" href="https://wa.me/6289651632799?text=Saya%20ingin%20bertanya"><i class="fab fa-whatsapp" aria-hidden="true"></i> Hubungi Penjual</a>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        
        <div class="row my-5">
            <div class="col-lg-12">
            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                <h2 style="display:contents;">Review Produk</h2> <span>({{ $count_review }} Ulasan)</span>
                </div>
                <div class="card-body">
                    @if($review->count() == 0)
                    <h3 class="text-center">Belum ada review produk</h3>
                    @endif
                    @foreach($review as $item)
                    <div class="media mb-3">
                        <div class="mr-2"> 
                            <img style="width:60px;"class="rounded-circle border p-1" src="{{ asset('assets/images/user1.png') }}">
                        </div>
                        <div class="media-body">
                            <p>{{ $item->content }}</p>
                        <small class="text-muted">Posted by {{ $item->customer['username'] }} on {{ date('d M Y', strtotime($item->created_at)) }}</small>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
                <div class="card-header">
                    <h2>Berikan Review Produk</h2>
                    </div>
                    @if (session()->has('successreview'))
                    <script>
                    Swal.fire({
                        title: 'Review Produk!',
                        text:  'Berhasil menambahkan review produk. Menunggu konfirmasi admin untuk publish',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                    @endif 
                    <div class="card-body">
                        @if (auth()->guard('customer')->check())
                    <form method="POST" action="{{ route('review') }}">
                        @csrf
                        <input type="hidden" name="customers_id" value="{{ Auth::guard('customer')->user()->id }}">
                        <input type="hidden" name="id_produk" value="{{ $product->id }}">
                        <label for="">Masukkan review anda</label>
                        <textarea class="form-control" rows="3" name="content" required></textarea>
                        <button type="submit" style="color:white; margin-top:20px;"class="btn hvr-hover">Leave a Review</button>
                    </form>
                        @else
                        <h3 class="text-center">
                            Silahkan login terlebih dulu untuk memberikan review.
                        </h3>
                        @endif
                    </div>
                
              </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col-lg-12">
                <div class="title-all text-center">
                    <h1>Produk Terkait</h1>
                    @if($lainnya->count() == 0)
                            <h3 class="title">Tidak ada produk yang terkait.</h3>
                    @endif
                </div>
                <div class="product-categorie-box">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade show active">
                            <div class="row">
                                @foreach($lainnya as $item)
                                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                                    <div class="products-single fix">
                                        <div class="box-img-hover">
                                            <div class="type-lb">
                                                @if($item->stok > 0)
                                                <p class="sale">Sale</p>
                                                @else
                                                <p class="sale">Out Of Stock</p>
                                                @endif
                                            </div>
                                            <img src="{{ url('img/'.$item->gambar) }}" class="img-fluid" alt="Image">
                                            <div class="mask-icon">
                                                <ul>
                                                    <li><a href="{{ url('detailproduct/'.$item->id) }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="View"><i class="fas fa-eye"></i></a></li>
                                                </ul>
                                                @if (auth()->guard('customer')->check())
                                                    <form method="POST" action="{{ route('cart')}}">
                                                        @csrf
                                                        <input type="hidden" name="id_produk" value="{{$item->id}}">
                                                        <input type="hidden" name="sub_harga" value="{{$item->harga}}">
                                                        <input type="hidden" name="jumlah" value="1">
                                                    <button class="cart" type="submit">Add to Cart</button>
                                                    </form>
                                                    @else
                                                    <a class="cart" onclick="reqLogin()" href="{{url('/customer/login')}}">Add to Cart</a>
                                                    <script>
                                                    function reqLogin() {
                                                    alert("Anda harus melakukan login terlebih dahulu");
                                                    }
                                                    </script>
                                                    @endif
                                            </div>
                                        </div>
                                        <div class="why-text">
                                        <h4>{{ $item->nama_produk }}</h4>
                                            <h5> Rp. {{ number_format($item->harga, 0) }}/{{ $item->satuan }}</h5>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- End Cart -->
@endsection