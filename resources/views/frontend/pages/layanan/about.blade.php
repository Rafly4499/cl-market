@extends('frontend.app') 
@section('pageTitle', 'About')
@section('content')
<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Tentang Kami</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Beranda</a></li>
                    <li class="breadcrumb-item active">Tentang Kami</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start About Page  -->
<div class="about-box-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="banner-frame"> <img class="img-fluid" src="{{ asset('img/bgtsabitahfood.jpg') }}" alt="" />
                </div>
            </div>
            <div class="col-lg-6">
                <h2 class="noo-sh-title-top">We are <span>Tsabitahfood</span></h2>
                <p>"Tsabitahfood, memiliki visi untuk memajukan UMKM di Indonesia demi mewujudkan pemerataan ekonomi. Dengan beberapa layanan yang kami tawarkan, kami memiliki ambisi untuk bisa membantu memajukan UMKM di Indonesia, seiring dengan misi jangka panjang Indonesia."</p>
                <div class="row my-5">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End About Page -->
@endsection