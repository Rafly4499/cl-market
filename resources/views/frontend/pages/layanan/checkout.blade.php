@extends('frontend.app') 
@section('pageTitle', 'Checkout')
@section('content')
<!-- Start All Title Box -->
<div class="all-title-box">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Checkout</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                    <li class="breadcrumb-item active">Checkout</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End All Title Box -->

<!-- Start Cart  -->
<div class="cart-box-main">
    <div class="container">
        
            <div class="col-sm-12 col-lg-12 mb-3">
        <form method="POST" action="{{ route('submitOrder')}}" class="needs-validation" novalidate>
            @csrf
            <div class="row">
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="checkout-address">
                    <div class="title-left">
                        <h3>Billing address</h3>
                    </div>
                    
                        <input type="hidden" name="id_customer" value="{{ Auth::guard('customer')->user()->id }}">
                        <input type="hidden" name="id_pesanan" value="{{ $pesanan->id }}">
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="firstName">Full name</label>
                                <input type="text" class="form-control" id="firstName" value="{{ Auth::guard('customer')->user()->fullname }}" disabled>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="username">Username</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="firstName" value="{{ Auth::guard('customer')->user()->username }}" disabled>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" id="firstName" value="{{ Auth::guard('customer')->user()->email }}" disabled>
                        </div>
                        <div class="mb-3">
                            <label for="email">No HP</label>
                            <input type="number" class="form-control" id="firstName" value="{{ Auth::guard('customer')->user()->no_hp }}" disabled>
                        </div>
                        <div class="mb-3">
                            <label for="address">Alamat Pengiriman</label>
                            <input type="text" class="form-control" id="address" name="alamat_pengiriman" placeholder="" required>
                            <div class="invalid-feedback"> Masukkan alamat pengiriman anda. </div>
                        </div>
                        <div class="mb-3">
                            <label for="address">Catatan Order</label>
                            <textarea class="form-control" name="catatan_order" id="" cols="30" rows="10"></textarea>
                            <div class="invalid-feedback"> Masukkan catatan order anda. </div>
                        </div>
                        <hr class="mb-1"> 
                    
                </div>
            </div>
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="odr-box">
                            <div class="title-left">
                            <h3>No Order : {{ $pesanan->no_order }}</h3>
                            </div>
                            @foreach($detpesanan as $item)
                            <div class="rounded p-2 bg-light">
                                <div class="media mb-2 border-bottom">
                                    <div class="col-md-3">
                                        <div class="thumbnail-img">
                                            <img class="img-fluid" src="{{ url('img/'.$item->produk['gambar']) }}" alt="" />
                                            </div>
                                    </div>
                                    <div class="col-md-9">
                                    <div class="media-body"> <a href="{{ url('detailproduct/'.$item->id) }}"> {{ $item->produk['nama_produk']}}</a>
                                            <div class="small text-muted">Price: Rp. {{ number_format($item->harga_satuan, 0) }} <span class="mx-2">|</span> Qty: {{ $item->quantity}} <span class="mx-2">|</span> Subtotal: Rp. {{ number_format($item->total_subharga, 0) }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="order-box">
                            <div class="title-left">
                                <h3>Your order</h3>
                            </div>
                            <div class="d-flex">
                                <div class="font-weight-bold">Product</div>
                                <div class="ml-auto font-weight-bold">Total</div>
                            </div>
                            <hr class="my-1">
                            <div class="d-flex">
                                <h4>Sub Total</h4>
                                <div class="ml-auto font-weight-bold"> Rp. {{ number_format($pesanan->total_harga, 0) }} </div>
                            </div>
                            <hr>
                            <div class="d-flex gr-total">
                                <h5>Grand Total</h5>
                                <div class="ml-auto h5"> Rp. {{ number_format($pesanan->total_harga, 0) }} </div>
                            </div>
                            <hr> </div>
                    </div>
                    <div class="col-12 d-flex shopping-box"> <button type="submit" class="ml-auto btn hvr-hover">Buat Pesanan</button> </div>
                </div>
            </div>
        </div>
        </form>
            </div>
        

    </div>
</div>
<!-- End Cart -->


@endsection