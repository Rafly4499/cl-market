@extends('frontend.app')
@section('pageTitle', 'Product')
@section('content')
<div class="osahan-profile">
    <div class="d-none">
        <div class="bg-primary border-bottom p-3 d-flex align-items-center">
            <a class="toggle togglew toggle-2" href="#"><span></span></a>
            <h4 class="font-weight-bold m-0 text-white">Product</h4>
        </div>
    </div>

    <div class="container position-relative">
        <div class="py-5 osahan-profile row">
            <div class="col-md-4 mb-3">
                <div class="bg-white rounded shadow-sm sticky_sidebar overflow-hidden">
                    <a href="profile.html" class="">
                        <div class="d-flex align-items-center p-3">
                            <div class="left mr-3">
                                <img alt="#" src="{{asset ('/frontend/img/user2.png')}}" class="rounded-circle">
                            </div>
                            <div class="right">
                                <h6 class="mb-1 font-weight-bold">{{ Auth::guard('customer')->user()->fullname }} <i
                                        class="feather-check-circle text-success"></i></h6>
                                <p class="text-muted m-0 small"><span class="__cf_email__">{{ Auth::guard('customer')->user()->email }}</span>
                                </p>
                            </div>
                        </div>
                    </a>

                    <div class="bg-white profile-details">
                        <a data-toggle="modal" data-target="#paycard"
                            class="d-flex w-100 align-items-center border-bottom p-3">
                            <div class="left mr-3">
                                <h6 class="font-weight-bold mb-1 text-dark">Update Data</h6>
                                <p class="small text-muted m-0">Edit Your Account</p>
                            </div>
                            <div class="right ml-auto">
                                <h6 class="font-weight-bold m-0"><i class="feather-chevron-right"></i></h6>
                            </div>
                        </a>
                        <a data-toggle="modal" data-target="#exampleModal"
                            class="d-flex w-100 align-items-center border-bottom p-3">
                            <div class="left mr-3">
                                <h6 class="font-weight-bold mb-1 text-dark">Product</h6>
                                <p class="small text-muted m-0">Kelola Produk Anda</p>
                            </div>
                            <div class="right ml-auto">
                                <h6 class="font-weight-bold m-0"><i class="feather-chevron-right"></i></h6>
                            </div>
                        </a>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-8 mb-3">
                <div class="">
                    @foreach($product as $item)
                    <div class="p-3 border-bottom menu-list">
                        <span class="float-right"><a href="#"
                                class="btn btn-outline-secondary btn-sm my-cart-btn" data-id="{{ $item->id }}"
                                data-name="{{ $item->nama_produk }}" data-summary="{{ $item->nama_produk }}"
                                data-price="{{ $item->harga }}" data-quantity="1"
                                data-image="{{ url('img/'.$item->gambar) }}">Tambah</a></span>
                        <div class="media">
                            <img alt="#" src="{{ url('img/'.$item->gambar) }}" class="mr-3 rounded-pill ">
                            <div class="media-body">
                                <h5 class="mb-1">{{ $item->nama_produk }}</h5>
                                <p class="pricecl mb-0">Rp. {{ number_format($item->harga, 0) }}</p>
                                <div class="list-card-badge">
                                    <span
                                        class="badge badge-success">{{ $item->kategori_produk['kategori_produk']}}</span>
                                </div>
                            </div>

                        </div>

                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="osahan-menu-fotter fixed-bottom bg-white px-3 py-2 text-center d-none">
        <div class="row">
            <div class="col selected">
                <a href="{{url('/')}}" class=" text-dark small font-weight-bold text-decoration-none">
                    <p class="h4 m-0"><i class="feather-home text-dark"></i></p>
                    Home
                </a>
            </div>
            <div class="col bg-white rounded-circle mt-n4 px-3 py-2">
                <div class="bg-danger rounded-circle mt-n0 shadow">
                    <a class="text-white small font-weight-bold text-decoration-none my-cart-icon">
                        <i class="feather-shopping-cart"></i>
                    </a>
                </div>
            </div>
            <div class="col">
                <a href="{{url('/users')}}" class="text-danger small font-weight-bold text-decoration-none">
                    <p class="h4 m-0"><i class="feather-user"></i></p>
                    Profile
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
