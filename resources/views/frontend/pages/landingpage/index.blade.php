@extends('frontend.app')
@section('pageTitle', 'Beranda')
@section('content')
<!-- Start Slider -->
<div class="osahan-home-page">
    <div class="bg-primary p-3 d-none">
        <div class="text-white">
            <div class="title d-flex align-items-center">
                <a class="toggle" href="#">
                    <span></span>
                </a>
                <h4 class="font-weight-bold m-0 pl-5">CampaignLab Market</h4>
            </div>
        </div>
        <div class="input-group mt-3 rounded shadow-sm overflow-hidden">
            <div class="input-group-prepend">
                <button class="border-0 btn btn-outline-secondary text-dark bg-white btn-block"><i
                        class="feather-search"></i></button>
            </div>
            <input type="text" class="shadow-none border-0 form-control" placeholder="Search for restaurants or dishes">
        </div>
    </div>

    <div class="container">
        <div class="cat-slider">
            @foreach($category as $item)
            <div class="cat-item px-1 py-3">
                <a class="bg-white rounded d-block p-2 text-center shadow-sm" href="{{ url('/category/'.$item->id) }}">
                    <h6 class="m-0 small">{{ $item->kategori_produk }}</h6>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="py-3 title d-flex align-items-center">
            <h5 class="m-0">Produk</h5>
        </div>

        <div class="most_popular">
            <div class="shadow-sm rounded bg-white mb-3 overflow-hidden">
                <div class="d-flex item-aligns-center">
                    <p class="font-weight-bold h6 p-3 border-bottom mb-0 w-100">Menu</p>

                </div>
                <div class="row m-0">
                    <div class="col-md-12 px-0 border-top">
                        <div class="">
                            @foreach($product as $item)
                            <div class="p-3 border-bottom menu-list">
                                <span class="float-right"><a href="#"
                                        class="btn btn-outline-secondary btn-sm my-cart-btn" data-id="{{ $item->id }}"
                                        data-name="{{ $item->nama_produk }}" data-summary="{{ $item->nama_produk }}"
                                        data-price="{{ $item->harga }}" data-quantity="1"
                                        data-image="{{ url('img/'.$item->gambar) }}">Tambah</a></span>
                                <div class="media">
                                    <img alt="#" src="{{ url('img/'.$item->gambar) }}" class="mr-3 rounded-pill ">
                                    <div class="media-body">
                                        <h5 class="mb-1">{{ $item->nama_produk }}</h5>
                                        <p class="pricecl mb-0">Rp. {{ number_format($item->harga, 0) }}</p>
                                        <div class="list-card-badge">
                                            <span
                                                class="badge badge-success">{{ $item->kategori_produk['kategori_produk']}}</span>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="osahan-menu-fotter fixed-bottom bg-white px-3 py-2 text-center d-none">
    <div class="row">
        <div class="col bg-white rounded-circle mt-n4 px-3 py-2">
            <div class="bg-danger rounded-circle mt-n0 shadow">
                <a class="text-white small font-weight-bold text-decoration-none my-cart-icon">
                    <i class="feather-shopping-cart"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col selected">
            <a href="{{url('/')}}" class="text-danger small font-weight-bold text-decoration-none">
                <p class="h4 m-0"><i class="feather-home text-dark"></i></p>
                Home
            </a>
        </div>
        <div class="col bg-white rounded-circle mt-n4 px-3 py-2">
            <div class="bg-danger rounded-circle mt-n0 shadow">
                <a class="text-white small font-weight-bold text-decoration-none my-cart-icon">
                    <i class="feather-shopping-cart"></i>
                </a>
            </div>
        </div>
        <div class="col">
            <a href="{{url('/users')}}" class="text-dark small font-weight-bold text-decoration-none">
                <p class="h4 m-0"><i class="feather-user"></i></p>
                Profile
            </a>
        </div>
    </div>
</div>
@endsection
