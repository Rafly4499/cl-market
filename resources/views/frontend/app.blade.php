<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Askbootstrap">
    <meta name="author" content="Askbootstrap">
    <link rel="icon" type="{{ asset('frontend/image/png" href="img/fav.png') }}">
    <title>Campaign Lab Market</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/slick/slick.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/slick/slick-theme.min.css') }}" />

    <link href="{{ asset('frontend/vendor/icons/feather.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('frontend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/glyphicon.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/vendor/sidebar/demo.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}"> --}}
</head>

<body class="fixed-bottom-bar">
    @include('frontend.partials.header')
    @yield('content') 
    @include('frontend.partials.footer')
    @include('frontend.partials.nav')

    
    <script type="356a15872f62a2fcbd489402-text/javascript" src="{{ asset('frontend/vendor/jquery/jquery.min.js') }}">
    </script>
    <script type="356a15872f62a2fcbd489402-text/javascript"
        src="{{ asset('frontend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <script type="356a15872f62a2fcbd489402-text/javascript" src="{{ asset('frontend/vendor/slick/slick.min.js') }}">
    </script>

    <script type="356a15872f62a2fcbd489402-text/javascript"
        src="{{ asset('frontend/vendor/sidebar/hc-offcanvas-nav.js') }}"></script>
    
    <script type="356a15872f62a2fcbd489402-text/javascript" src="{{ asset('frontend/js/osahan.js') }}"></script>
    <script src="{{ asset('frontend/js/rocket-loader.min.js') }}"
        data-cf-settings="356a15872f62a2fcbd489402-|49" defer=""></script>
    <script defer src="{{ asset('frontend/js/beacon.min.js') }}"
        data-cf-beacon='{"si":10,"r":1,"rayId":"62cafe83bc6621e5","version":"2021.2.0"}'></script>
        <script src="{{ asset('frontend/js/jquery-2.2.3.min.js') }}"></script>
        <script type='text/javascript' src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
        <script type='text/javascript' src="{{ asset('frontend/js/jquery.mycart.js') }}"></script>
        <script type="text/javascript">
            $(function () {
          
              var goToCartIcon = function($addTocartBtn){
                var $cartIcon = $(".my-cart-icon");
                var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
                $addTocartBtn.prepend($image);
                var position = $cartIcon.position();
                $image.animate({
                  top: position.top,
                  left: position.left
                }, 500 , "linear", function() {
                  $image.remove();
                });
              }
          
              $('.my-cart-btn').myCart({
                currencySymbol: 'Rp.',
                classCartIcon: 'my-cart-icon',
                classCartBadge: 'my-cart-badge',
                classProductQuantity: 'my-product-quantity',
                classProductRemove: 'my-product-remove',
                classCheckoutCart: 'my-cart-checkout',
                affixCartIcon: true,
                showCheckoutModal: true,
                cartItems: [
                ],
                clickOnAddToCart: function($addTocart){
                  goToCartIcon($addTocart);
                },
                afterAddOnCart: function(products, totalPrice, totalQuantity) {
                  console.log("afterAddOnCart", products, totalPrice, totalQuantity);
                },
                clickOnCartIcon: function($cartIcon, products, totalPrice, totalQuantity) {
                  console.log("cart icon clicked", $cartIcon, products, totalPrice, totalQuantity);
                },
                checkoutCart: function(products, totalPrice, totalQuantity) {
                  var checkoutString = "Total Price: " + totalPrice + "\nTotal Quantity: " + totalQuantity;
                  checkoutString += "\n\n id \t name \t summary \t price \t quantity \t image path";
                  $.each(products, function(){
                    checkoutString += ("\n " + this.id + " \t " + this.name + " \t " + this.summary + " \t " + this.price + " \t " + this.quantity + " \t " + this.image);
                  });
                  alert(checkoutString)
                  console.log("checking out", products, totalPrice, totalQuantity);
                },
              });
          
            //   $("#addNewProduct").click(function(event) {
            //     var currentElementNo = $(".row").children().length + 1;
            //     $(".row").append('<div class="col-md-3 text-center"><img src="images/img_empty.png" width="150px" height="150px"><br>product ' + currentElementNo + ' - <strong>$' + currentElementNo + '</strong><br><button class="btn btn-danger my-cart-btn" data-id="' + currentElementNo + '" data-name="product ' + currentElementNo + '" data-summary="summary ' + currentElementNo + '" data-price="' + currentElementNo + '" data-quantity="1" data-image="images/img_empty.png">Add to Cart</button><a href="#" class="btn btn-info">Details</a></div>')
            //   });
            });
            </script>
</body>
</html>
