<aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item selected"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/dashboard') }}"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Produk</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/admin-ds/produk') }}"
                                aria-expanded="false"><i class="icon-layers"></i><span
                                    class="hide-menu">List Produk
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/jenisproduk') }}"
                            aria-expanded="false"><i data-feather="calendar" class="feather-icon"></i><span
                                class="hide-menu">Jenis Produk</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/kategoriproduk') }}"
                                aria-expanded="false"><i class="icon-tag"></i><span
                                    class="hide-menu">Kategori Produk</span></a></li>
                                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/review') }}"
                                        aria-expanded="false"><i data-feather="bar-chart" class="feather-icon"></i><span
                                            class="hide-menu">Review Produk</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Transaksi</span></li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/pesanan') }}"
                                aria-expanded="false"><i class="icon-basket-loaded"></i><span
                                    class="hide-menu">Pemesanan </span></a>
                        </li>
                        
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Management</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/customer') }}"
                                aria-expanded="false"><i class="icon-people"></i><span
                                    class="hide-menu">Customer
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link"
                                href="{{ url('/admin-ds/mitra') }}" aria-expanded="false"><i class="icon-emotsmile"></i><span class="hide-menu">Mitra
                                </span></a>
                        </li>

                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/toko') }}"
                                aria-expanded="false"><i class="icon-home"></i><span
                                    class="hide-menu">Toko Mitra
                                </span></a>
                        </li>
                        
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Utilities</span></li>
                        {{-- <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/contactus') }}"
                            aria-expanded="false"><i class="icon-call-end"></i><span
                                class="hide-menu">Contact Us</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/admin') }}"
                                aria-expanded="false"><i class="icon-social-reddit"></i><span
                                    class="hide-menu">Admin</span></a></li> --}}
                        
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/admin-ds/logout') }}"
                                aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">Logout</span></a></li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>