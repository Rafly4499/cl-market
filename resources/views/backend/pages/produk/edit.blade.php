@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Produk</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/produk') }}" class="text-muted">Produk</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Produk</h4>
                                <a href="{{ url('/admin-ds/produk') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <form action="{{ url('/admin-ds/produk/'.$produk->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="fullname">Jenis Produk</label>
                            <select class="form-control select2" value="{{ $produk->jenis_produk['jenis_produk'] }}" name="id_jenis_produk" id="" required>
                    @foreach($jenis_produk as $data)
                        <option value="{{ $data->id }}">{{ $data->jenis_produk }}</option>
                    @endforeach
                    </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                        <label for="fullname">Kategori Produk</label>
                        <select class="form-control select2" value="{{ $produk->kategori_produk['kategori_produk'] }}" name="id_kategori_produk" id="" required>
                @foreach($kategori_produk as $data)
                    <option value="{{ $data->id }}">{{ $data->kategori_produk }}</option>
                @endforeach
                </select>
                    </div>
                </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Nama Produk</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="nama_produk" value="{{ $produk->nama_produk }}">
                                        </div>
                                </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Deskripsi</label>
                                            <div class="col-md-12">
                                            <input type="text" class="form-control" name="deskripsi" value="{{ $produk->deskripsi }}">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Satuan</label>
                                            <div class="col-md-12">
                                                <select class="form-control select2" name="satuan" value="{{ $produk->satuan }}" id="" required>
                                                    <option value="buah" {{ $produk->satuan == 'buah' ? 'selected' : '' }}>Buah</option>
                                                    <option value="pcs" {{ $produk->satuan == 'pcs' ? 'selected' : '' }}>Pcs</option>
                                                    <option value="ekor" {{ $produk->satuan == 'ekor' ? 'selected' : '' }}>Ekor</option>
                                                    <option value="gram" {{ $produk->satuan == 'gram' ? 'selected' : '' }}>Gram</option>
                                                    <option value="ons" {{ $produk->satuan == 'ons' ? 'selected' : '' }}>Ons</option>
                                                    <option value="pon" {{ $produk->satuan == 'pon' ? 'selected' : '' }}>Pon</option>
                                                    <option value="kg" {{ $produk->satuan == 'kg' ? 'selected' : '' }}>Kilogram</option>
                                                    <option value="kwintal" {{ $produk->satuan == 'kwintal' ? 'selected' : '' }}>Kwintal</option>
                                                    <option value="ton" {{ $produk->satuan == 'ton' ? 'selected' : '' }}>Ton</option>
                                                
                                                </select>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Stok</label>
                                            <div class="col-md-12">
                                            <input type="number" class="form-control" name="stok" value="{{ $produk->stok }}">
                                            </div>
                                    </div>
                                    
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Harga</label>
                                            <div class="col-md-12">
                                            <input type="number" class="form-control" name="harga" value="{{ $produk->harga }}">
                                            </div>
                                    </div>
                                    {{-- <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="fullname">Toko Penjual</label>
                                        <select class="form-control select2" value="{{ $produk->toko['nama_toko'] }}" name="id_toko" id="" required>
                                @foreach($toko as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama_toko }}</option>
                                @endforeach
                                </select>
                                    </div>
                                </div> --}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="fullname">Status</label>
                                        <select class="form-control select2" value="{{ $produk->status }}" name="status" id="" required>
                                
                                    <option value="1" {{ $produk->status == 1 ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ $produk->status == 0 ? 'selected' : '' }}>Pending</option>
                                
                                </select>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Gambar Produk</label>
                                        <div class="col-md-12" style="margin-top: 10px">
                                        <input type="file" name="image" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="{{ $produk->gambar }}"/>     
                                        </div>
                                    </div>
                                    <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                                    <a href="{{ url('/admin-ds/produk') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection