@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Produk</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Produk</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Produk</h4>
                                <a href="{{ url('/admin-ds/produk') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Jenis Produk</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$produk->jenis_produk['jenis_produk']}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Kategori Produk</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$produk->kategori_produk['kategori_produk']}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Produk</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$produk->nama_produk}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Deskripsi</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$produk->deskripsi}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Satuan</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$produk->satuan}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Stok</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$produk->stok}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Harga</label>
                                                        
                                                        <div class="col-md-8"><p>:@currency($produk->harga)</p> </div>
                                                    </div>
                                                </div>
                                    
                                   <a href="{{ url('/admin-ds/produk/'.$produk->id.'/edit') }}"><button type="submit" name="submit" class="btn btn-info">UPDATE</button></a> 
                                     
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                            <div class="form-group">
                                        <label for="" class="col-md-6 control-label">Gambar Produk</label>
                                    </div>
                                <div class="col-md-12">
                                <img style="border-radius: 5px; width:100%" src="{{ url('img/'.$produk->gambar) }}" alt="">
                                </div>
                            
                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection