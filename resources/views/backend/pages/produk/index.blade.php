@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Produk</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">List Produk</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List Produk</h4>
                                <a href="{{ url('/admin-ds/produk/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>  Tambah Data</button></a>
                                <div class="table-responsive">
                                    <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Jenis Produk</th>
                                                <th>Kategori Produk</th>
                                                <th>Nama Produk</th>
                                                <th>Stok</th>
                                                
                                                <th>Status</th>
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 0;?>
                                        @foreach($produk as $item)
                                        <?php $no++;?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $item->jenis_produk->jenis_produk }}</td>
                                            <td>{{ $item->kategori_produk->kategori_produk }}</td>
                                            <td>{{ $item->nama_produk }}</td>
                                            <td>{{ $item->stok }}</td>
                                            
                                            <td>
                                                @if($item->status == 0)
                                                    <button class="btn btn-rounded btn-warning">Pending</button>
                                                @else
                                                <button class="btn btn-rounded btn-success">Active</button>
                                                @endif
                                            </td>
                                            <td>
                                                <form action="{{ url('/admin-ds/produk/'.$item->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <a href="{{ url('/admin-ds/produk/'.$item->id.'/edit') }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="{{ url('/admin-ds/produk/'.$item->id.'/view') }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                            <th>No</th>
                                            <th>Jenis Produk</th>
                                            <th>Kategori Produk</th>
                                            <th>Nama Produk</th>
                                            <th>Stok</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            @endsection