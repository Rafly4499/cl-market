@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Metode Pembayaran</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/metodepembayaran') }}" class="text-muted">Metode Pembayaran</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Metode Pembayaran</h4>
                               
                                <form action="{{ url('/admin-ds/metodepembayaran/'.$metode->id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Metode Pembayaran</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" value="{{ $metode->nama_metode }}" name="nama_metode">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Deskripsi</label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" id="summary-ckeditor" rows="3" name="deskripsi">{{ $metode->deskripsi }}</textarea>
                                        </div>
                                    </div>
                                    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                                    <script>
                                        CKEDITOR.replace('summary-ckeditor');

                                    </script>
                                    <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                                    <a href="{{ url('/admin-ds/metodepembayaran') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection