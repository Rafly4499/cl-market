@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Toko Mitra</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/toko') }}" class="text-muted">Toko Mitra</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Toko</h4>
                                <a href="{{ url('/admin-ds/toko') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Penjual</label>
                                                        
                                                        <div class="col-md-8"><p>:{{ $toko->mitra['nama_lengkap']}} </p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Toko</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$toko->nama_toko}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Alamat</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$toko->alamat_toko}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Deskripsi</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$toko->deskripsi}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Penilaian</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$toko->penilaian}}</p> </div>
                                                    </div>
                                                </div>
                                                
                                               
                                    
                                   <!-- <a href="{{ url('/admin-ds/mitra/'.$mitra->id.'/edit') }}"><button type="submit" name="submit" class="btn btn-info">UPDATE</button></a>  -->
                                     
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                            <div class="form-group">
                                        <label for="" class="col-md-6 control-label">Foto Toko</label>
                                    </div>
                                <div class="col-md-12">
                                <img style="border-radius: 5px; width:100%" src="{{ url('img/'.$toko->photo) }}" alt="">
                                </div>
                            
                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection