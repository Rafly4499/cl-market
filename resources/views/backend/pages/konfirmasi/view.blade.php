@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Booking</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Booking</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Booking</h4>
                                <a href="{{ url('/panel/booking') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                               
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">No Booking</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$booking->bookingid}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Pemesan</label>
                                                        
                                                        <div class="col-md-8"><p>:{{ $booking->name_order }}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Asal Kota</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$booking->city}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Metode Pembayaran</label>
                                                        
                                                        <div class="col-md-8"><p>: {{$booking->method_payment}}</p> </div>
                                                    </div>
                                                </div>
                                                @if($booking->type_payment_bank)
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Tipe Pembayaran Bank</label>
                                                        
                                                        <div class="col-md-8"><p>: {{ $booking->type_payment_bank }}</p> </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @if($booking->status == 'Proses')
                                                <a href="{{ url('/panel/booking/approval/'.$booking->id.'/confirmation') }}"><button class="btn btn-success">Konfirmasi Terima</button></a>
                                                <a href="{{ url('/panel/booking/rejected/'.$booking->id.'/confirmation') }}"><button class="btn btn-warning">Tidak Sesuai</button></a> 
                                                @endif
                                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Status Booking</label>
                                        
                                        <div class="col-md-8"><p>:{{$booking->status}}</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Jumlah partisipan</label>
                                        
                                        <div class="col-md-8"><p>: {{$booking->number_participant}}</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Harga</label>
                                        
                                        <div class="col-md-8"><p>:Rp. {{ number_format($booking->payment, 0) }},- </p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Total Pembayaran</label>
                                        
                                        <div class="col-md-8"><p>:Rp. {{ number_format($booking->total_payment, 0) }},-</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Tanggal Booking</label>
                                        
                                        <div class="col-md-8"><p>:{{ date('d M Y', strtotime($booking->date_booking)) }}</p> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection