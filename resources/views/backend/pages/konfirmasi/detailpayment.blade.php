@extends('layouts.admin') 
@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Booking</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Booking</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Konfirmasi Pembayaran</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Konfirmasi Pembayaran</h4>
                                <a href="{{ url('/panel/booking') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                               
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">No Rekening</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$confirmation->transfer_destination}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Pemilik Rekening</label>
                                                        
                                                        <div class="col-md-8"><p>:{{ $confirmation->account_owner }}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">No Handphone</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$confirmation->no_hp}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Tanggal Pembayaran</label>
                                                        
                                                        <div class="col-md-8"><p>: {{ date('d M Y', strtotime($confirmation->date_payment)) }} </p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Total Pembayaran</label>
                                                        
                                                        <div class="col-md-8"><p>: Rp. {{ number_format($confirmation->total_payment, 0) }},- </p> </div>
                                                    </div>
                                                </div>
                                                @if($booking->status == 'Proses')
                                                <a href="{{ url('/panel/booking/approval/'.$booking->id) }}"><button class="btn btn-success">Konfirmasi Terima</button></a>
                                                <a href="{{ url('/panel/booking/rejected/'.$booking->id) }}"><button class="btn btn-warning">Tidak Sesuai</button></a> 
                                                @endif
                                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        
                        <div class="card">
                            <div class="card-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Status Booking</label>
                                        
                                        <div class="col-md-8"><p>:{{$booking->status}}</p> </div>
                                    </div>
                                </div>
                                        <label for="" class="col-md-6 control-label">Bukti Pembayaran</label>
                                    </div>
                                <div class="col-md-12">
                                <?php
                                    if($confirmation->image_confirmation){
                                ?>
                                <img style="border-radius: 5px; width:100%" src="{{ url('admin/img/konfirmasi_pembayaran/'.$confirmation->image_confirmation) }}" alt="">
                                <?php
                                    }else{
                                ?>
                                <h6>Belum Ada Data Bukti Pembayaran</h6>
                                <?php
                                    }
                                ?>
                                </div>
                            
                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection