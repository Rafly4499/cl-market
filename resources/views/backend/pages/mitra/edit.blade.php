@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Produk</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/produk') }}" class="text-muted">Produk</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tambah Data Produk</h4>
                                <a href="{{ url('/admin-ds/produk') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <form action="{{ url('/admin-ds/produk/'.$produk->id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                        
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="fullname">Jenis Produk</label>
                            <select class="form-control select2" value="{{ $produk->jenis_produk['jenis_produk'] }}" name="id_jenis_produk" id="" required>
                    @foreach($jenis_produk as $data)
                        <option value="{{ $data->id }}">{{ $data->jenis_produk }}</option>
                    @endforeach
                    </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                    <div class="form-group">
                        <label for="fullname">Kategori Produk</label>
                        <select class="form-control select2" value="{{ $produk->kategori_produk['kategori_produk'] }}" name="id_kategori_produk" id="" required>
                @foreach($kategori_produk as $data)
                    <option value="{{ $data->id }}">{{ $data->kategori_produk }}</option>
                @endforeach
                </select>
                    </div>
                </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Nama Produk</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="nama_produk" value="{{ $produk->nama_produk }}">
                                        </div>
                                </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Deskripsi</label>
                                            <div class="col-md-12">
                                            <input type="text" class="form-control" name="deskripsi" value="{{ $produk->deskripsi }}">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Berat</label>
                                            <div class="col-md-12">
                                            <input type="number" class="form-control" name="berat" value="{{ $produk->berat }}">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Stok</label>
                                            <div class="col-md-12">
                                            <input type="number" class="form-control" name="stok" value="{{ $produk->stok }}">
                                            </div>
                                    </div>
                                    
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Harga</label>
                                            <div class="col-md-12">
                                            <input type="number" class="form-control" name="harga" value="{{ $produk->harga }}">
                                            </div>
                                    </div>
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="fullname">Toko Penjual</label>
                                        <select class="form-control select2" value="{{ $produk->toko['nama_toko'] }}" name="id_toko" id="" required>
                                @foreach($toko as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama_toko }}</option>
                                @endforeach
                                </select>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Gambar Produk</label>
                                        <div class="col-md-12" style="margin-top: 10px">
                                        <input type="file" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="{{ $produk->gambar }}"/>     
                                        </div>
                                    </div>
                                    <button type="submit" name="submit" class="btn btn-info">SUBMIT</button>
                                    <button type="reset" class="btn btn-dark">RESET</button></a>
                                    <input type="hidden" name="_method" value="post"> 
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection