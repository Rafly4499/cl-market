@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Mitra</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/mitra') }}" class="text-muted">mitra</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Tambah Data mitra</h4>
                                <a href="{{ url('/admin-ds/mitra') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <form class="form-body" method="post" action="{{ url('/admin-ds/mitra/') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                    
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Nama Lengkap</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="nama_lengkap">
                                        </div>
                                </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Username</label>
                                            <div class="col-md-12">
                                            <input type="text" class="form-control" name="name">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Email</label>
                                            <div class="col-md-12">
                                            <input type="email" class="form-control" name="email">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">Password</label>
                                            <div class="col-md-12">
                                            <input type="password" class="form-control" name="password">
                                            </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Confirm Password</label>
                                        <div class="col-md-12">
                                        <input type="password" class="form-control" name="c_password">
                                        </div>
                                </div>
                                    
                                    <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">No Handphone</label>
                                            <div class="col-md-12">
                                            <input type="number" class="form-control" name="no_hp">
                                            </div>
                                    </div>
                                    <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="fullname">Jenis Kelamin</label> <br>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" value="1" id="customControlValidation1" name="sex">
                                                <label class="custom-control-label" style="margin-right:28px;" for="customControlValidation1">Laki - Laki</label>
                                                <input type="radio" class="custom-control-input" value="2" id="customControlValidation2" name="sex">
                                                <label class="custom-control-label" for="customControlValidation2">Perempuan</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nama Toko</label>
                                    <div class="col-md-12">
                                    <input type="text" class="form-control" name="nama_toko">
                                    </div>
                            </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Photo mitra</label>
                                        <div class="col-md-12" style="margin-top: 10px">
                                                <input type="file" class="form-control-file" name="image">      
                                        </div>
                                    </div>
                                    <button type="submit" name="submit" class="btn btn-info">SUBMIT</button>
                                    <button type="reset" class="btn btn-dark">RESET</button></a>
                                    <input type="hidden" name="_method" value="post"> 
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection