@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Review Produk</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/review') }}" class="text-muted">Review Produk</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Status</h4>
                                <a href="{{ url('/admin-ds/review') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <form action="{{ url('/admin-ds/review/'.$review->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="fullname">Status</label>
                                        <select class="form-control select2" value="{{ $review->status }}" name="status" id="" required>
                                
                                    <option value="1" {{ $review->status == 1 ? 'selected' : '' }}>Diizinkan</option>
                                    <option value="0" {{ $review->status == 0 ? 'selected' : '' }}>Pending</option>
                                
                                </select>
                                    </div>
                                </div>
                                    <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                                    <a href="{{ url('/admin-ds/review') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection