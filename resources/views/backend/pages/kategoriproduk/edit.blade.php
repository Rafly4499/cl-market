@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Produk</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/kategoriproduk') }}" class="text-muted">Kategori Produk</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Kategori Produk</h4>
                               
                                <form action="{{ url('/admin-ds/kategoriproduk/'.$kategori->id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="fullname">Jenis Produk</label>
                                        <select class="form-control select2" value="{{ $kategori->jenis_produk['jenis_produk'] }}" name="jenis_produk_id" id="" required>
                                @foreach($jenis_produk as $data)
                                    <option value="{{ $data->id }}">{{ $data->jenis_produk }}</option>
                                @endforeach
                                </select>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Kategori Produk</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" value="{{ $kategori->kategori_produk }}" name="kategori_produk">
                                        </div>
                                </div>
                                    <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                                    <a href="{{ url('/admin-ds/kategoriproduk') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection