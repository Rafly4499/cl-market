@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Produk</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Kategori Produk</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List Produk</h4>
                                <a href="{{ url('/admin-ds/kategoriproduk/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>  Tambah Data</button></a>
                                <div class="table-responsive">
                                    <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Jenis</th>
                                                <th>Kategori Produk</th>
                                                <th>Action</th>
                                                <th></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 0;?>
                                        @foreach($kategori as $item)
                                        <?php $no++;?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $item->jenis_produk['jenis_produk'] }}</td>
                                            <td>{{ $item->kategori_produk }}</td>
                                            <td>
                                                <form action="{{ url('/admin-ds/kategoriproduk/'.$item->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <a href="{{ url('/admin-ds/kategoriproduk/'.$item->id.'/edit') }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
                                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        
                                        @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Jenis</th>
                                                <th>Kategori Produk</th>
                                                <th>Action</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            @endsection