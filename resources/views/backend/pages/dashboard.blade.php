@extends('backend.app')
@section('content')

<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Hello Admin Tsabitahfood!</h3>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-5 align-self-center">
                        <div class="customize-input float-right">
                            <select class="custom-select custom-select-set form-control bg-white border-0 custom-shadow custom-radius">
                                <option selected>Thn 2020</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- *************************************************************** -->
                <!-- Start First Cards -->
                <!-- *************************************************************** -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-hover">
                            <div class="p-2 bg-primary text-center">
                                <h1 class="font-light text-white">{{ $countuser }}</h1>
                                <h6 class="text-white">Total Pengguna Tsabitahfood</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-hover">
                            <div class="p-2 bg-cyan text-center">
                                <h1 class="font-light text-white">{{ $countproduk }}</h1>
                                <h6 class="text-white">Total Produk Terdaftar</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-hover">
                            <div class="p-2 bg-success text-center">
                                <h1 class="font-light text-white">{{ $countorder }}</h1>
                                <h6 class="text-white">Total Transaksi Pemesanan</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3 col-xlg-3">
                        <div class="card card-hover">
                            <div style="background-color: #63ef6e!important;" class="p-2 text-center">
                                <h1 class="font-light text-white">{{ $countmitra }}</h1>
                                <h6 class="text-white">Total Mitra</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <h4>Pengguna Tsabitahfood</h4>
                <div class="card-group">
                    
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">{{ $countcustomer }}</h2>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Total Customer</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:60px;"src="{{ asset('assets/images/marketplace/005-businessman.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">{{ $countmitra }}</h2>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Total Mitra
                                    </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:60px;"src="{{ asset('assets/images/marketplace/004-hand shake.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">{{ $counttoko }}</h2>
                                    
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Toko Mitra Terdaftar</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:60px;"src="{{ asset('assets/images/marketplace/030-online store.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <h2 class="text-dark mb-1 font-weight-medium">{{ $countusernonaktif }}</h2>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pengguna Non Aktif</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:60px;"src="{{ asset('assets/images/marketplace/003-megaphone.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h4>Transaksi Pemesanan</h4>
                <div class="card-group">
                    
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">{{ $countordermasuk }}</h2>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pesanan Masuk</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:50px;"src="{{ asset('assets/images/marketplace/010-mobile shopping.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">{{ $countorderkirim }}</h2>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pesanan Proses Kirim
                                    </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:50px;"src="{{ asset('assets/images/marketplace/016-truck.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">{{ $countorderselesai }}</h2>
                                        
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pesanan Selesai</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:50px;"src="{{ asset('assets/images/marketplace/027-cash.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <h2 class="text-dark mb-1 font-weight-medium">{{ $countorderbatal }}</h2>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pesanan Dibatalkan</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:50px;"src="{{ asset('assets/images/marketplace/018-mobile shopping.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h4>Produk Tsabitahfood</h4>
                <div class="card-group">
                    
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">{{ $countprodukaktif }}</h2>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Produk Aktif</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:50px;"src="{{ asset('assets/images/logistik/014-files.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">{{ $countproduktidakaktif }}</h2>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Produk Pending
                                    </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:50px;"src="{{ asset('assets/images/logistik/007-computer.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">{{ $countcategory }}</h2>
                                        
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Jumlah Kategori Produk</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <img style="width:50px;"src="{{ asset('assets/images/marketplace/025-online shop.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <h2 class="text-dark mb-1 font-weight-medium">{{ $countjenis }}</h2>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Jumlah Jenis Produk</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                     <img style="width:50px;"src="{{ asset('assets/images/marketplace/006-store.png')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                </div>
                
@endsection