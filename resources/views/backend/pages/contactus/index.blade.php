@extends('backend.app') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Contact Us</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Kontak Kami</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Kontak Kami</h4>
                    
                    <div class="table-responsive">
                        <table id="default_order" class="table table-striped table-bordered display no-wrap" style="width:100%">
                            <thead>
                                
                                <tr>
                                    <th>No</th>
                                    <th>Nama Lengkap</th>
                                    <th>No Handphone</th>
                                    <th>Subject Pesan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;?> @foreach($contact as $item)
                                <?php $no++;?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $item->first_name }}{{ $item->last_name }}</td>
                                    <td>{{ $item->phone_number }}</td>
                                    <td>{{ $item->subject }}</td>

                                    <td>
                                        <form action="{{ url('/admin-ds/contactus/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="{{ url('/admin-ds/contactus/'.$item->id.'/view') }}" class="btn btn-info"><i class="fas fa-eye"></i> View</a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </td>
                                    <td></td>
                                </tr>

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                <th>No</th>
                                <th>Nama Lengkap</th>
                                <th>No Handphone</th>
                                <th>Subject Pesan</th>
                                <th>Action</th>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
