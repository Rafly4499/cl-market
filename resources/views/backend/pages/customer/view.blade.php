@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Customer</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/admin-ds/customer') }}" class="text-muted">Customer</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Customer</h4>
                                <a href="{{ url('/admin-ds/customer') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Depan</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->nama_depan}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Belakang</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->nama_belakang}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Username</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->user['name']}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Email</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->user['email']}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Belakang</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->nama_belakang}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Alamat</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->alamat}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Jenis Kelamin</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->sex}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">No Handphone</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->no_hp}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Tanggal Lahir</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$customer->birth_date}}</p> </div>
                                                    </div>
                                                </div>
                                               
                                    
                                   <!-- <a href="{{ url('/admin-ds/customer/'.$customer->id.'/edit') }}"><button type="submit" name="submit" class="btn btn-info">UPDATE</button></a>  -->
                                     
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                            <div class="form-group">
                                        <label for="" class="col-md-6 control-label">Photo Customer</label>
                                    </div>
                                <div class="col-md-12">
                                <img style="border-radius: 5px; width:100%" src="{{ url('img/'.$customer->photo) }}" alt="">
                                </div>
                            
                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection