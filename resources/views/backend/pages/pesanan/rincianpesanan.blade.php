@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Rincian Pesanan</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/pesanan') }}" class="text-muted">List Pesanan</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">No Order : {{ $pesanan['no_order'] }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Rincian Pesanan</h4>
                                <a href="{{ url('/admin-ds/pesanan') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <div class="table-responsive">
                                    <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Produk</th>
                                                <th>Jumlah Produk</th>
                                                <th>Harga Satuan Produk</th>
                                                <th>Total Harga Produk</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 0;?>
                                        @foreach($rincianpesanan as $item)
                                        <?php $no++;?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>
                                                @if($item->id_produk != NULL)
                                                    {{ $item->produk['nama_produk'] }}
                                                @elseif($item->id_lelang_produk != NULL)
                                                    {{ $item->lelang['nama_produk'] }}
                                                @endif
                                            </td>
                                            <td>{{ $item->quantity }}</td>
                                            <td>@currency($item->harga_satuan)</td>
                                            <td>@currency($item->total_subharga)</td>
                                        </tr>
                                        @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Produk</th>
                                                <th>Jumlah Produk</th>
                                                <th>Harga Satuan Produk</th>
                                                <th>Total Harga Produk</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            @endsection