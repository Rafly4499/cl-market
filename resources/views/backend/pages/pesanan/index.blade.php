@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Pesanan</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="{{ url('/admin-ds/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">List Pesanan</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List Pesanan</h4>
                                
                                <div class="table-responsive">
                                    <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No Order</th>
                                                <th>Nama Customer</th>
                                                <th>Status</th>
                                                <th>Tanggal Order</th>
                                                <th>Total Harga</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 0;?>
                                        @foreach($pesanan as $item)
                                        <?php $no++;?>
                                        <tr>
                                            <td>{{ $item->no_order }}</td>
                                            <td>{{ $item->customer['fullname'] }}</td>
                                            
                                            <td>
                                                @if($item->status == 'Selesai')
                                                    <button class="btn btn-rounded btn-sm btn-success">Selesai</button>
                                                @elseif($item->status == 'Dibatalkan')
                                                <button class="btn btn-rounded btn-sm btn-danger">Dibatalkan</button>
                                                @elseif($item->status == 'Belum Bayar')
                                                <button class="btn btn-rounded btn-sm btn-warning">Belum Bayar</button>
                                                @elseif($item->status == 'Dikemas')
                                                <button class="btn btn-rounded btn-sm btn-light">Dikemas</button>
                                                @elseif($item->status == 'Dikirim')
                                                <button class="btn btn-rounded btn-sm btn-secondary">Dikirim</button>
                                                @else
                                                <button class="btn btn-rounded btn-sm btn-info">{{ $item->status }}</button>
                                                @endif
                                            </td>
                                            <td>{{$item->date_order}}</td>
                                            <td>@currency($item->total_harga)</td>
                                            <td>
                                                    <a href="{{ url('/admin-ds/pesanan/'.$item->id.'/view') }}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                                    <a href="{{ url('/admin-ds/pesanan/'.$item->id.'/rincianpesanan') }}" class="btn btn-success btn-sm"><i class="fas fa-shopping-cart"></i></a>
                                                    @if($item->status == 'Proses' || $item->status == 'Sudah Bayar')
                                                    <a href="{{ url('/admin-ds/pesanan/'.$item->id.'/detailpayment') }}" class="btn btn-secondary btn-sm"><i class="fas fa-clipboard-check"></i></a>
                                                    @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No Order</th>
                                                <th>Nama Customer</th>
                                                <th>Status</th>
                                                <th>Tanggal Order</th>
                                                <th>Total Harga</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            @endsection