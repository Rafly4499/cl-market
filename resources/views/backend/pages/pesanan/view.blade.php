@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Pesanan</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="index.html" class="text-muted">Pesanan</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Pesanan</h4>
                                <a href="{{ url('/admin-ds/pesanan') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                               
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">No Order</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$pesanan->no_order}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Customer</label>
                                                        
                                                        <div class="col-md-8"><p>:{{ $pesanan->customer['fullname'] }}</p> </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Alamat Pengantaran</label>
                                                        
                                                        <div class="col-md-8"><p>: {{$pesanan->alamat_pengiriman}}</p> </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Catatan Order</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$pesanan->catatan_order}}</p> </div>
                                                    </div>
                                                </div>
                                    
                                 
                                     
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Status Order</label>
                                        
                                        <div class="col-md-8"><p>:{{$pesanan->status}}</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Total Harga Produk</label>
                                        
                                        <div class="col-md-8"><p>:@currency($pesanan->total_hargaproduk)</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Total Pembayaran</label>
                                        
                                        <div class="col-md-8"><p>:@currency($pesanan->total_harga)</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Tanggal Order</label>
                                        
                                        <div class="col-md-8"><p>:{{$pesanan->date_order}}</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Tanggal Pembayaran</label>
                                        
                                        <div class="col-md-8"><p>:{{$pesanan->date_payment}}</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Tanggal Order Selesai</label>
                                        
                                        <div class="col-md-8"><p>:{{$pesanan->date_done}}</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Batas Tanggal Pembayaran</label>
                                        
                                        <div class="col-md-8"><p>:{{$pesanan->date_limit}}</p> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection